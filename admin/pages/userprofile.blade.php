@extends('SMVA.admin.layouts.adminlayout')
@section('content')

<div class="container-fluid content-wrapper">
   <div class=" content think-user-profile">
   <!-- <div class="col-md-5"> -->

            <!-- Profile Image -->
            <div class="box box-widget widget-user">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header bg-aqua-active">
              
            </div>
            <div class="widget-user-image">
              <img class="img-circle" src="thinkdashboard/dist/img/user1-128x128.jpg" alt="User Avatar">
            </div>

           

            <div class="box-footer">
              <!-- name of user -->
               <div class="think-user-name text-center">
                  <h3 class="widget-user-username">Alexander Pierce</h3>
                  <h5 class="widget-user-desc">Founder &amp; CEO</h5>
               </div>
               <hr>
              <div class="row">
               
                <div class="col-sm-4 border-right">
                  <div class="description-block">
                    <h5 class="description-header">3,200</h5>
                    <span class="description-text">SALES</span>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-4 border-right">
                  <div class="description-block">
                    <h5 class="description-header">13,000</h5>
                    <span class="description-text">FOLLOWERS</span>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-4">
                  <div class="description-block">
                    <h5 class="description-header">35</h5>
                    <span class="description-text">PRODUCTS</span>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
            </div>
          </div>
          <!-- /.widget-user -->

            <!-- About Me Box -->
            <div class="box box-primary">
            <div class="box-header with-border">
            <h3 class="box-title">About Me</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
             <div class="row">
               <div class="col-md-6">
                    <strong><i class="fa fa-book margin-r-5"></i> Education</strong>

                    <p class="text-muted">
                        B.S. in Computer Science from the University of Tennessee at Knoxville
                    </p>
               </div>
               <!-- /.col -->
              
               <div class="col-md-6">
                    <strong><i class="fa fa-map-marker margin-r-5"></i> Location</strong>

                   <p class="text-muted">Malibu, California</p>
               </div>
               <!-- /.col -->

             </div> 
             <!-- /. row -->
            <hr>

            <strong><i class="fa fa-pencil margin-r-5"></i> Skills</strong>

            <p>
                  <span class="label label-danger">UI Design</span>
                  <span class="label label-success">Coding</span>
                  <span class="label label-info">Javascript</span>
                  <span class="label label-warning">PHP</span>
                  <span class="label label-primary">Node.js</span>
            </p>

            <hr>

            <strong><i class="fa fa-file-text-o margin-r-5"></i> Notes</strong>

            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam fermentum enim neque.</p>
            </div>
            <!-- /.box-body -->
            </div>
            <!-- /.box -->
      <!-- </div> -->
      <!-- /.col -->

   </div>
  <!-- /.row -->

</div>
<!-- /.container -->







@endsection