@extends('SMVA.admin.layouts.adminlayout')
@section('content')

<div class="container-fluid content">

      <div class="row ">
         <div class="col-xs-12">
         <div class="box">
            <div class="box-header">
              <h3 class="box-title">All users</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>User Name</th>
                  <th>Email</th>
                  <th>Phone number</th>
                  <th>Address</th>
                  <th>Edit</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                  <td>Misc</td>
                  <td>Dillo 0.8</td>
                  <td>Embedded devices</td>
                  <td>-</td>
                  <td>
                  <!-- Edit delete -->
                  <a href="#" class = "btn btn-sm btn-primary">edit</a>
                  <a href="#" class = "btn btn-sm btn-danger">delete</a>
                  </td>
                </tr>
                <tr>
                  <td>Misc</td>
                  <td>Links</td>
                  <td>Text only</td>
                  <td>-</td>
                  <td>
                  <!-- Edit delete -->
                  <a href="#" class = "btn btn-sm btn-primary">edit</a>
                  <a href="#" class = "btn btn-sm btn-danger">delete</a>
                  </td>
                </tr>
                <tr>
                  <td>Misc</td>
                  <td>Lynx</td>
                  <td>Text only</td>
                  <td>-</td>
                  <td>
                  <!-- Edit delete -->
                  <a href="#" class = "btn btn-sm btn-primary">edit</a>
                  <a href="#" class = "btn btn-sm btn-danger">delete</a>
                  </td>
                </tr>
                <tr>
                  <td>Misc</td>
                  <td>IE Mobile</td>
                  <td>Windows Mobile 6</td>
                  <td>-</td>
                  <td>
                  <!-- Edit delete -->
                  <a href="#" class = "btn btn-sm btn-primary">edit</a>
                  <a href="#" class = "btn btn-sm btn-danger">delete</a>
                  </td>
                </tr>
                <tr>
                  <td>Misc</td>
                  <td>PSP browser</td>
                  <td>PSP</td>
                  <td>-</td>
                  <td>
                  <!-- Edit delete -->
                  <a href="#" class = "btn btn-sm btn-primary">edit</a>
                  <a href="#" class = "btn btn-sm btn-danger">delete</a>
                  </td>
                </tr>
                <tr>
                  <td>Other browsers</td>
                  <td>All others</td>
                  <td>-</td>
                  <td>-</td>
                  <td>
                  <!-- Edit delete -->
                  <a href="#" class = "btn btn-sm btn-primary">edit</a>
                  <a href="#" class = "btn btn-sm btn-danger">delete</a>
                  </td>
                </tr>
                </tbody>
                
              </table>
              </div>
            <!-- /.box-body -->
            </div>
          <!-- /.box -->
           
              <nav aria-label="Page navigation example">
                  <ul class="pagination justify-content-end pull-right">
                    <li class="page-item disabled">
                      <a class="page-link" href="#" tabindex="-1">Previous</a>
                    </li>
                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item">
                      <a class="page-link" href="#">Next</a>
                    </li>
                  </ul>
               </nav>

          
         

         </div>
           <!-- /.col -->
         
           




      </div>
      <!-- /.row -->
   </div>
<!-- /.conatiner -->





@endsection