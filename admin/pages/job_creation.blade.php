@extends('SMVA.admin.layouts.adminlayout')
@section('content')


<div class="content">
<div class = "container-fluid " >
 <div class="smva-job-create">
    <!-- SMVA HEADING -->
    <!-- <h2 class = "text-center smva-heading">SMVA Consultancy</h3> -->
  
    <div class=" card smva-card">
 
     <div class="card-body">
      <!-- Headings -->
      <h2 class="login-box-msg text-center">Job Create</h2>
    

    <form method="post">
     
      <div class="form-group has-feedback">
        <input type="text" class="form-control" name="date" placeholder="Date">
      </div>

      <div class="form-group has-feedback">
        <input type="text" class="form-control" name="job_title"  placeholder="Job Title">  
      </div>
     
      <div class="form-group has-feedback">
        <input type="text" class="form-control" name="client_name"  placeholder="Client Name">  
      </div>
     
      <div class="form-group has-feedback">
        <input type="text" class="form-control" name="client_description"  placeholder="Client Description">  
      </div>

      <div class="form-group has-feedback">
        <input type="text" class="form-control" name="job_description"  placeholder="Job Description">  
      </div>
     
      <div class="form-group has-feedback">
        <input type="text" class="form-control" name="location"  placeholder="Location">  
      </div>
     
       
      <div class="form-group has-feedback">
        <input type="text" class="form-control" name="qualification" placeholder="Qualification">
      </div>

      <div class="form-group has-feedback">
        <input type="text" class="form-control" name="role"  placeholder="Role">  
      </div>
     
      <div class="form-group has-feedback">
        <input type="text" class="form-control" name="role_description"  placeholder="Role Description">  
      </div>
     
      <div class="form-group has-feedback">
        <input type="text" class="form-control" name="skills_must_have"  placeholder="Skills Must Have">  
      </div>

      <div class="form-group has-feedback">
        <input type="text" class="form-control" name="key_responsibilities"  placeholder="Key Responsibilities">  
      </div>
     
      <div class="form-group has-feedback">
        <input type="text" class="form-control" name="technical_experience"  placeholder="Technical Experience">  
      </div>
     
  
      <div class="form-group has-feedback">
        <input type="text" class="form-control" name="professional_attribute" placeholder="Professional Attribute">
      </div>

      <div class="form-group has-feedback">
        <input type="text" class="form-control" name="experince"  placeholder="Experince">  
      </div>
     
      <div class="form-group has-feedback">
        <input type="text" class="form-control" name="annual_salary"  placeholder="Annual Salary">  
      </div>
     
      <div class="form-group has-feedback">
        <input type="text" class="form-control" name="payment_terms"  placeholder="Payment Terms">  
      </div>
     
      <div class="form-group has-feedback">
        <input type="text" class="form-control" name="replacement_clause"  placeholder="Replacement Clause">  
      </div>
     

      <!-- Submit button -->
        <div class="submit">
          <button type="submit" class="btn btn-primary btn-block">Create</button>
        </div>
        <!-- /.submit -->
    
    </form>

    
  </div>
  <!-- /.card-body -->
</div>
<!-- /.card -->
</div>
</div>
<!-- /.container-fluid -->

</div>

 <!-- /.content -->
@endsection