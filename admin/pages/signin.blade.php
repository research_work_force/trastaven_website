@extends('SMVA.admin.layouts.loginlayout')
@section('content')

<div class = "container-fluid " >
 <div class="smva-login-content">
    <!-- SMVA HEADING -->
    <h2 class = "text-center smva-heading" >SMVA Consultancy</h3>
  
    <div class=" card smva-card">
 
     <div class="card-body">
  
      <h3 class="login-box-msg text-center">Login</h3>
    

    <form method="post">
     
      <div class="form-group has-feedback">
        <input type="text" class="form-control" name="username" placeholder="Username">
        <!-- <span class="glyphicon glyphicon-user form-control-feedback"></span> -->
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" name = "password" placeholder="Password">
        <!-- <span class="glyphicon glyphicon-lock form-control-feedback"></span> -->
      </div>
     
         <!-- <div class="checkbox icheck">
            <label>
              <input type="checkbox"> I agree to the <a href="#">terms</a>
            </label>
          </div> -->
        
        <div class="submit">
          <button type="submit" class="btn btn-primary btn-block">Login</button>
        </div>
        <!-- /.submit -->
    
    </form>
<!-- 
    <div class="social-auth-links text-center">
      <p>- OR -</p>
      <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign up using
        Facebook</a>
      <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign up using
        Google+</a>
    </div> -->

    
  </div>
  <!-- /.card-body -->
</div>
<!-- /.card -->
</div>
</div>
<!-- /.container-fluid -->

@endsection