
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="SMVA/assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="SMVA/assets/img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    Paper Dashboard 2 by Creative Tim
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
  <!-- CSS Files -->
  <link href="SMVA/assets/css/bootstrap.min.css" rel="stylesheet" />
  <link href="SMVA/assets/css/paper-dashboard.css?v=2.0.0" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href="SMVA/assets/demo/demo.css" rel="stylesheet" />
  <link rel="stylesheet" href="SMVA/assets/css/style.css">