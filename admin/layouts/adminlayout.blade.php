<!DOCTYPE html>
<html lang="en">
<head>
      @include('SMVA.admin.include.headerlinks')
      <title>Admin</title>
</head>
<body class="">
   <div class="wrapper">
   @include('SMVA.admin.include.mainsidebar')
   
    <div class="main-panel">
    
    @include('SMVA.admin.include.navbar')

     @yield('content')

     @include('SMVA.admin.include.footer')

     
    
     </div>
    
    
     </div>
    

     
</body>
     
</html>