<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EduLoan extends Model
{
    protected $table = 'edu_loan';
    public $timestamps = false;
}
