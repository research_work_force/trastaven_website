<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AffiliatedBy extends Model
{
    protected $table = 'affiliated_by-opt';
    public $timestamps = false;
}
