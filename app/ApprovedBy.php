<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApprovedBy extends Model
{
    protected $table = 'approved_by';
    public $timestamps = false;
}
