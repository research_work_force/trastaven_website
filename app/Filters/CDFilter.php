<?php
namespace App\Filters;

class CDFilter
{
     public function filter($builder, $value)
    {
        return $builder->where('certificate_and_degrees', $value);
    }
}