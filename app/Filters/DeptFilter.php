<?php
namespace App\Filters;

class DeptFilter
{
     public function filter($builder, $value)
    {
        return $builder->where('dept', $value);
    }
}