<?php
namespace App\Filters;

class LocFilter
{
     public function filter($builder, $value)
    {
        return $builder->where('location', $value);
    }
}