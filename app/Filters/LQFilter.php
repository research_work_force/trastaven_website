<?php
namespace App\Filters;

class LQFilter
{
     public function filter($builder, $value)
    {
        return $builder->where('last_qualification', $value);
    }
}