<?php
namespace App\Filters;

use App\Filters\AbstractFilter;
use Illuminate\Database\Eloquent\Builder;

class CourseFilter extends AbstractFilter
{
    protected $filters = [
        'course_type' => CTypeFilter::class,
        'course_fees' => CFeesFilter::class,
        'last_qualification' => LQFilter::class,
        'course_and_program' => CPFilter::class,
        'dept' => DeptFilter::class,
        'certificate_and_degrees' => CDFilter::class,
        'location' => LocFilter::class,
        'affiliated_by' => AffFilter::class,
        'approved_by' => AppFilter::class,
        'eduloan' => EduLoanFilter::class,
        'trating' => TRatingFilter::class
    ];
}