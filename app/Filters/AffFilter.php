<?php
namespace App\Filters;

class AffFilter
{
     public function filter($builder, $value)
    {
        return $builder->where('affiliated_by', $value);
    }
}