<?php
namespace App\Filters;

class CPFilter
{
     public function filter($builder, $value)
    {
        return $builder->where('course_and_program', $value);
    }
}