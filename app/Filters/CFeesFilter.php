<?php
namespace App\Filters;

class CFeesFilter
{
     public function filter($builder, $value)
    {
        return $builder->where('course_fees', $value);
    }
}