<?php
namespace App\Filters;

class CTypeFilter
{
     public function filter($builder, $value)
    {
        return $builder->where('course_type', $value);
    }
}