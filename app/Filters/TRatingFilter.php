<?php
namespace App\Filters;

class TRatingFilter
{
     public function filter($builder, $value)
    {
        return $builder->where('trating', $value);
    }
}