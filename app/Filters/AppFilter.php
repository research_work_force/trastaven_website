<?php
namespace App\Filters;

class AppFilter
{
     public function filter($builder, $value)
    {
        return $builder->where('approved_by', $value);
    }
}