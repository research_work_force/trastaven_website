<?php
namespace App\Filters;

class EduLoanFilter
{
     public function filter($builder, $value)
    {
        return $builder->where('eduloan', $value);
    }
}