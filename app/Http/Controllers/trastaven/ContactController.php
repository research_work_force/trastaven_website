<?php

namespace App\Http\Controllers\trastaven;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Posts;
use App\Users;
use App\ContactForm;

class ContactController extends Controller
{

    public function showContactList()
    {
        return view('trastaven.pages.contactlist');
    }

    public function showLoanPage()
    {
        return view('trastaven.frontend.pages.loan');
    }

    public function saveContacts(Request $req)
    {
    	$name = $req->input('name');
		$ph = $req->input('ph');
		$email = $req->input('email');
		$msg = $req->input('msg');

		$contact_data = new ContactForm();

		$contact_data->name = $name;
		$contact_data->phone = $ph;
		$contact_data->email = $email;
		$contact_data->msg = $msg;
		$contact_data->save();

		return redirect()->back();


    }

}