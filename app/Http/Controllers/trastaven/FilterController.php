<?php

namespace App\Http\Controllers\trastaven;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Posts;
use App\Users;
use App\LastQualification;
use App\AffiliatedBy;
use App\ApprovedBy;
use App\CertificatesDegrees;
use App\CoursesPrograms;
use App\CourseType;
use App\Department;
use App\EduLoan;
use App\Location;
use App\FeesRange;


class FilterController extends Controller
{

    public static function CourseType()
    {
    	$CourseType = CourseType::all();
        return $CourseType;
    }

    public static function LastQualification()
    {
    	$LastQualification = LastQualification::all();
        return $LastQualification;
    }

     public static function CoursesPrograms()
    {
    	$CoursesPrograms = CoursesPrograms::all();
        return $CoursesPrograms;
    }

    public static function Department()
    {
    	$Department = Department::all();
        return $Department;
    }

    public static function CertificatesDegrees()
    {
    	$CertificatesDegrees = CertificatesDegrees::all();
        return $CertificatesDegrees;
    }

    public static function Location()
    {
    	$Location = Location::all();
        return $Location;
    }

    public static function AffiliatedBy()
    {
    	$AffiliatedBy = AffiliatedBy::all();
        return $AffiliatedBy;
    }

    public static function ApprovedBy()
    {
    	$ApprovedBy = ApprovedBy::all();
        return $ApprovedBy;
    }

    public static function EduLoan()
    {
    	$EduLoan = EduLoan::all();
        return $EduLoan;
    }

    public static function FeesRange()
    {
    	$FeesRange = FeesRange::all();
        return $FeesRange;
    }

    public function addSubFilters()
    {
       return view('trastaven.pages.add_subfilters');
    }

    public function saveSubFilters(Request $req)
    {
       $filter_types = $req->input('filter_types');
       $sub_filtername = ucfirst($req->input('sub_filtername'));

       switch($filter_types){
        case '1':
                 $course = new CourseType();
                 $course->name = $sub_filtername;
                 $course->save();
                 
                 break;
        case '2':
                 $course = new FeesRange();
                 $course->name = $sub_filtername;
                 $course->save();
                 break;
        case '3':
                 $course = new LastQualification();
                 $course->name = $sub_filtername;
                 $course->save();
                 break;
        case '4':
                 $course = new CoursesPrograms();
                 $course->name = $sub_filtername;
                 $course->save();
                 break;
        case '5':
                 $course = new Department();
                 $course->name = $sub_filtername;
                 $course->save();
                 break;
        case '6':
                 $course = new CertificatesDegrees();
                 $course->name = $sub_filtername;
                 $course->save();
                 break;
        case '7':
                 $course = new Location();
                 $course->name = $sub_filtername;
                 $course->save();
                 break;
        case '8':
                 $course = new AffiliatedBy();
                 $course->name = $sub_filtername;
                 $course->save();
                 break;
        case '9':
                 $course = new ApprovedBy();
                 $course->name = $sub_filtername;
                 $course->save();
                 break;
        case '10':
                 $course = new EduLoan();
                 $course->name = $sub_filtername;
                 $course->save();
                 break;
       }

       return redirect()->back();


    }

    public function showFiltersCategoryList()
    {
        return view('trastaven.pages.filter_category_list');
    }

    public function showFiltersByCategory(Request $req)
    {
        $filter_types = $req->input('filter_types');
        $flag = '0';

       switch($filter_types){
        case '1':
                 $course = CourseType::orderBy('id', 'desc')->paginate(10); 
                 $flag = '1';             
                 break;
        case '2':
                 $course = FeesRange::orderBy('id', 'desc')->paginate(10);
                 $flag = '2';

                 break;
        case '3':
                 $course = LastQualification::orderBy('id', 'desc')->paginate(10);
                 $flag = '3';
                 break;
        case '4':
                 $course = CoursesPrograms::orderBy('id', 'desc')->paginate(10);
                 $flag = '4';
                 
                 break;
        case '5':
                 $course = Department::orderBy('id', 'desc')->paginate(10);
                 $flag = '5';
                 break;
        case '6':
                 $course = CertificatesDegrees::orderBy('id', 'desc')->paginate(10);
                 $flag = '6';
                 break;
        case '7':
                 $course = Location::orderBy('id', 'desc')->paginate(10);
                 $flag = '7';
                 break;
        case '8':
                 $course = AffiliatedBy::orderBy('id', 'desc')->paginate(10);
                 $flag = '8';
                 break;
        case '9':
                 $course = ApprovedBy::orderBy('id', 'desc')->paginate(10);
                 $flag = '9';
                 break;
        case '10':
                 $course = EduLoan::orderBy('id', 'desc')->paginate(10);
                 $flag = '10';
                 break;
       }


                return view('trastaven.pages.filters_list')
                          ->with('course_filter',$course)
                          ->with('flag',$flag); 
                 

    }

    public function subFiltersDelete($id,$flag)
    {

       switch($flag){
        case '1':
                 $course = CourseType::find($id); 
                 $course->delete();           
                 break;
        case '2':
                 $course = FeesRange::find($id); 
                 $course->delete();
                 
                 break;
        case '3':
                 $course = LastQualification::find($id); 
                 $course->delete();
                 break;
        case '4':
                 $course = CoursesPrograms::find($id); 
                 $course->delete();
                 break;
        case '5':
                 $course = Department::find($id); 
                 $course->delete();
                 break;
        case '6':
                 $course = CertificatesDegrees::find($id); 
                 $course->delete();
                 break;
        case '7':
                 $course = Location::find($id); 
                 $course->delete();
                 break;
        case '8':
                 $course = AffiliatedBy::find($id); 
                 $course->delete();
                 break;
        case '9':
                 $course = ApprovedBy::find($id); 
                 $course->delete();
                 break;
        case '10':
                 $course = EduLoan::find($id); 
                 $course->delete();
                 break;
       }

       return redirect('/filters/category/list');
    }

}