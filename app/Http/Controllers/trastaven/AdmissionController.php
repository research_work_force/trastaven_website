<?php

namespace App\Http\Controllers\trastaven;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Posts;
use App\Users;
use App\AdmissionRequest;
use App\Courses;
use Spatie\QueryBuilder\QueryBuilder;

class AdmissionController extends Controller
{
    public function admissionList()
    {
        $admission_req_data = AdmissionRequest::orderBy('date', 'desc')->paginate(10);

        return view('trastaven.pages.admissionreqlist',compact('admission_req_data'));
    }

    public function showInputCoursesPage()
    {
        return view('trastaven.pages.inputcollegeinfo');
    }

    public function saveCourses(Request $req)
    {
        $course_type = $req->input('course_type');
        $course_fees = $req->input('course_fees');
        $last_qualification = $req->input('last_qualification');
        $course_and_program = $req->input('course_and_program');
        $dept = $req->input('dept');
        $certificate_and_degrees = $req->input('certificate_and_degrees');
        $location = $req->input('location');
        $affiliated_by = $req->input('affiliated_by');
        $approved_by = $req->input('approved_by');
        $eduloan = $req->input('eduloan');
        $college_name = $req->input('college_name');
        $place = $req->input('place');
        $trating = $req->input('trating');
        $details = $req->input('details');
     
        $time = Carbon::now();

        $course_ob = new Courses();
        $course_ob->course_type = $course_type;
        $course_ob->course_fees = $course_fees;
        $course_ob->last_qualification = $last_qualification;
        $course_ob->course_and_program = $course_and_program;
        $course_ob->dept = $dept;
        $course_ob->certificate_and_degrees = $certificate_and_degrees;
        $course_ob->location = $location;
        $course_ob->place = $place;
        $course_ob->affiliated_by = $affiliated_by;
        $course_ob->approved_by = $approved_by;
        $course_ob->eduloan = $eduloan;
        $course_ob->college_name = $college_name;
        $course_ob->trating = $trating;
        $course_ob->details = $details;
        $course_ob->created_at = $time;
        $course_ob->updated_at = $time;

        $course_ob->save();
        \Session::flash('message', "Course has been created"); 
         \Session::flash('alert-class', 'alert-success');
        return redirect()->back();


    }

    public function showCourses()
    {
        $course_data = Courses::orderBy('created_at', 'desc')->paginate(10);

        return view('trastaven.pages.courseslist',compact('course_data'));
    }

    public function editCourses($id)
    {
        $course_data = Courses::where('id',$id)->first();

        return view('trastaven.pages.editcourse',compact('course_data'));
    }

    public function updateCourses(Request $req)
    {
        $course_name = $req->input('course_name');
        $location = $req->input('location');
        $college_name = $req->input('college_name');
        $affiliated_to = $req->input('affiliated_to');
        $approved_by = $req->input('approved_by');
        $under_which_group = $req->input('under_which_group');
        $eligibility_criteria = $req->input('eligibility_criteria');
        $trastaven_gradetion = $req->input('trastaven_gradetion');
        $first_yr = $req->input('first_yr');
        $second_yr = $req->input('second_yr');
        $third_yr = $req->input('third_yr');
        $fourth_yr = $req->input('fourth_yr');
        $id = $req->input('id');
        $time = Carbon::now();

        $course_data_checking = Courses::find($id);


        $course_data_checking->course_name = $course_name;
        $course_data_checking->location = $location;
        $course_data_checking->college_name = $college_name;
        $course_data_checking->affiliated_to = $affiliated_to;
        $course_data_checking->approved_by = $approved_by;
        $course_data_checking->under_w_group = $under_which_group;
        $course_data_checking->eligibility_criteria = $eligibility_criteria;
        $course_data_checking->trastaven_gradetion = $trastaven_gradetion;
        $course_data_checking->first_yr_fees = $first_yr;
        $course_data_checking->second_yr_fees = $second_yr;
        $course_data_checking->third_yr_fees = $third_yr;
        $course_data_checking->fourth_yr_fees = $fourth_yr;
        $course_data_checking->updated_at = $time;
        $course_data_checking->save();

        \Session::flash('message', "Course has been updated"); 
         \Session::flash('alert-class', 'alert-success');
        
         return redirect()->back();
    }

    public function listOfCourses()
    {
        $course_data = Courses::orderBy('created_at', 'desc')->paginate(10);

        return view('trastaven.frontend.pages.courses',compact('course_data'));
    }

    public function Courses(Request $request)
    {
      // $course_data = Courses::filter($request)->get();

      // $course_data = QueryBuilder::for(Courses::class)
      //                 ->allowedFilters(['course_type', 'course_fees'])
      //                 ->get();

         $course_data = Courses::orWhere('course_type', $request->course_type)
                                  ->orWhere('course_fees',$request->fees_range)
                                  ->orWhere('last_qualification',$request->last_qualification)
                                  ->orWhere('course_and_program',$request->course_program)
                                  ->orWhere('dept',$request->dept)
                                  ->orWhere('certificate_and_degrees',$request->certificate_and_degrees)
                                  ->orWhere('location',$request->location)
                                  ->orWhere('affiliated_by',$request->affiliated_by)
                                  ->orWhere('approved_by',$request->approved_by)
                                  ->orWhere('eduloan',$request->edu_loan)
                                  ->orWhere('trating',$request->five)
                                  ->orWhere('trating',$request->four)
                                  ->orWhere('trating',$request->three)
                                  ->orWhere('trating',$request->two)
                                  ->orWhere('trating',$request->one)
                                  ->paginate(15);


      return view('trastaven.frontend.pages.courses',compact('course_data'));
    }

    public function deleteCourses($id)
    {
        $post = Courses::find($id);

        $post->delete();
        \Session::flash('message', "Course has been deleted"); 
         \Session::flash('alert-class', 'alert-warning');
          return redirect()->back();
    }

    public function frontpageAdmissionCoursesList(Request $req)
    {
         $name = $req->input('name');
         $email = $req->input('email');
         $ph = $req->input('ph');
         $address = $req->input('address');
         $msg = $req->input('msg');
         $clgname = $req->input('clgname');
         $coursename = $req->input('coursename');
         $id = $req->input('id');

         $time = Carbon::now();

         $admission_req_data = new AdmissionRequest();
         $admission_req_data->name = $name;
         $admission_req_data->email = $email;
         $admission_req_data->phone = $ph;
         $admission_req_data->address = $address;
         $admission_req_data->msg = $msg;
         $admission_req_data->institute_code = $clgname;
         $admission_req_data->course_code = $coursename;
         $admission_req_data->course_id = $id;
         $admission_req_data->date = $time;

         $admission_req_data->save();

         return redirect()->back();

    }
}
