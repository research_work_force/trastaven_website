<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LastQualification extends Model
{
    protected $table = 'last_qualification';
    public $timestamps = false;
}
