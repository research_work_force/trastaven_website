<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Filters\CourseFilter;
use Illuminate\Database\Eloquent\Builder;

class Courses extends Model
{
    protected $table = 'courses';


     public function scopeFilter(Builder $builder, $request)
    {
        return (new CourseFilter($request))->filter($builder);
    }
}
 