<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CoursesPrograms extends Model
{
   protected $table = 'courses_programs';
   public $timestamps = false;
}
