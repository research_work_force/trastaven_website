<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CertificatesDegrees extends Model
{
     protected $table = 'certificate_degrees';
     public $timestamps = false;
}
