<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FeesRange extends Model
{
    protected $table = 'fees';
    public $timestamps = false;
}
