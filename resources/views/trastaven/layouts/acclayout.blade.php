 <!DOCTYPE html>
<html>
 @include('trastaven.include.headerlinks')
<body class="bg-trastaven">

 @yield('content')

 @include('trastaven.include.loginfooter')
 @include('trastaven.include.footerlinks')
</body>

</html>