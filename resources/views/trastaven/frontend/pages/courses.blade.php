@extends('trastaven.frontend.layouts.placementlayout')
@section('content')


      <!-- Search Input -->
      <div class="container-fluid main-search mt-3 ">
            <div class="input mb-3">
                        <!-- <label for="Admissions">Search:</label> -->
                        <div class="input-group search mb-3">        
                        <input type="text" class="form-control" placeholder="Recipient's username" aria-label="Recipient's username" aria-describedby="basic-addon2">
                        <div class="input-group-append">
                              <span class="input-group-text text-white" id="basic-addon2">Search</span>
                        </div>
                        </div>
            </div>
                        
                            
      </div>      

<!-- Filter Section -->
<div class="container-fluid trastaven-filter-page">
            <div class="row trastaven-row">


                @include('trastaven.frontend.include.categories')
@php
$AffiliatedBy = App\Http\Controllers\trastaven\FilterController::AffiliatedBy();
$ApprovedBy = App\Http\Controllers\trastaven\FilterController::ApprovedBy();
$CertificatesDegrees = App\Http\Controllers\trastaven\FilterController::CertificatesDegrees();
$CoursesPrograms = App\Http\Controllers\trastaven\FilterController::CoursesPrograms();
$CourseType = App\Http\Controllers\trastaven\FilterController::CourseType();
$Department = App\Http\Controllers\trastaven\FilterController::Department();
$EduLoan = App\Http\Controllers\trastaven\FilterController::EduLoan();
$FeesRange = App\Http\Controllers\trastaven\FilterController::FeesRange();
$LastQualification = App\Http\Controllers\trastaven\FilterController::LastQualification();
$Location = App\Http\Controllers\trastaven\FilterController::Location();

@endphp
                        <!-- Search Result -->
                        <div class="col-sm-12 col-lg-6 search-reasult">
                        @if($course_data != null)

                        @foreach($course_data as $data)                         
                              <div class="result mb-2">
                              <h3>{{ $data->college_name }}</h3>
                              <h4 class="font-weight-bold">{{ $data->details }}</h4>

                                    <p class="location-p m-0 font-weight-bold"><i class="fas fa-map-marker-alt"></i>
                                    @foreach ($Location as $l)
                                      @if($l->id == $data->location )
                                      {{ $l->name }} 
                                      @endif
                                    @endforeach


                              <h6><strong>Course Fees:</strong>
                                  @foreach ($FeesRange as $l)
                                      @if($l->id == $data->course_fees )
                                      {{ $l->name }} 
                                      @endif
                                    @endforeach</h6>

                              <h6><strong>Course program:</strong>
                                  @foreach ($CoursesPrograms as $l)
                                      @if($l->id == $data->course_and_program )
                                      {{ $l->name }} 
                                      @endif
                                    @endforeach</h6>


                                @for($i = 0; $i < intval($data->trating) ;$i++)
                                  <i class="fas fa-star" aria-hidden="true"></i>
                                @endfor

                                    </p> 
                                        <!-- Button trigger modal -->
                                          <button type="button" class="btn btn-trastaven" data-toggle="modal" data-target="#exampleModalCenter{{$data->id}}">
                                          Enquery Now
                                          </button>
                                 </div>
                                  @include('trastaven.frontend.pages.admissionreqmodal',['clgname'=>$data->college_name,'coursename'=>$data->course_and_program,'id'=>$data->id])
                              @endforeach 
                                                            

                              @endif
                              
                  
                              </div>

                              {{-- right side bar --}}

                              <div class="col-md-3">

                              </div>
                              
                        </div>
                        <!-- pagination -->
                        <div class="container">
                        <nav aria-label="Page navigation example">
                                    <!-- <ul class="pagination justify-content-end">
                                          <li class="page-item disabled">
                                          <a class="page-link" href="#" tabindex="-1">Previous</a>
                                          </li>
                                          <li class="page-item"><a class="page-link" href="#">1</a></li>
                                          <li class="page-item"><a class="page-link" href="#">2</a></li>
                                          <li class="page-item"><a class="page-link" href="#">3</a></li>
                                          <li class="page-item">
                                          <a class="page-link" href="#">Next</a>
                                          </li>
                                    </ul> -->
                                            {{ $course_data->render("pagination::bootstrap-4") }}
                                    
                                    </nav>

                        </div>
    
                        
                      

</div>

<!-- popup modals -->
      <!-- Button trigger modal -->
<!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">
      Launch demo modal
    </button> -->
    
    <!-- Modal -->
    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Inquery Now</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form>
                  <div class="form-group">
                    <!-- Name -->
                    <label for="recipient-name" class="col-form-label">Name:</label>
                    <input type="text" class="form-control" id="recipient-name">
                    <!-- Email -->
                    <label for="recipient-email" class="col-form-label">Email Id:</label>
                    <input type="text" class="form-control" id="recipient-email">
                    <!-- phone number -->
                    <label for="recipient-phone-number" class="col-form-label">Phone Number:</label>
                    <input type="text" class="form-control" id="recipient-phone-number">
                  <!-- Address -->
                  <label for="recipient-address" class="col-form-label">Address:</label>
                  <input type="text" class="form-control" id="recipient-address">
                <!-- messege -->
                <label for="recipient-phone-number" class="col-form-label">Messege:</label>
                <input type="text" class="form-control" id="recipient-Messege">

                  </div>
                </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            <button type="button" class="btn save-btn btn-trastaven">Save changes</button>
          </div>
        </div>
      </div>
    </div>




    




@endsection