@extends('trastaven.frontend.layouts.placementlayout')


@section('content')




<div class="container-fluid mt-3">
   <div class="row services" id ="placement">
      <div class="col-md-6 col-sm-12">
      <div class="card" id ="think-card">
                      
                      <div class="card-body text-center">
                        
                              <img src="{{asset('trastaven/frontend/images/icons/admissionindia.png')}}"  class="img-fluid" alt="Responsive image">
                    
                           <div class="card-heading">
                           <h5>Abroad Placement</h5>
                           <!-- <hr class="think-hr-sky"> -->
                           </div>

                        <div class="training-details">

                             <p class="text-left">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quasi, rem vitae fugiat exercitationem ducimus asperiores sequi praesentium amet deserunt facilis sed suscipit quo eveniet laborum qui libero eaque cum blanditiis?</p>

                          <!-- <ul>
                          <li>It Sector</li>
                          <li>Social Care</li>
                          <li>Banking Sector</li>
                          </ul>  -->


                                <div class="details-btn text-center">
                                <a href="#" class="btn btn-sm btn-trastaven">VIEW DETAILS</a>
                                </div>
                        
                        </div>
                      </div>
                    </div> 
                   <!--card  -->
      </div>
      <!-- /.col -->

      <div class="col-md-6 col-sm-12">
      <div class="card" id ="think-card">
                      <div class="card-body text-center">
                        
                              <img src="{{asset('trastaven/frontend/images/icons/admissionindia.png')}}" class="img-fluid" alt="Responsive image">
                    
                           <div class="card-heading">
                           <h5>Pan India Placement</h5>
                           <!-- <hr class="think-hr-sky"> -->
                           </div>
                        <div class="training-details">

                        <p class="text-left">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Quos sit atque dolorum similique doloribus officiis commodi quis, est accusamus velit hic necessitatibus, odit doloremque deserunt corrupti illum blanditiis non delectus.</p>


                        <!-- <ul>
                          <li> Industrial Sector</li>
                          <li>Banking Sector</li>
                          <li>It Sector</li>
                          <li>Education Sector</li>
                          <li>Marketing Sector</li>
                          <li>Advertising Sector</li>
                          <li>BPO, KPO</li>
                          <li>Tourism Sector</li>
                          <li>Hotels</li>
                          <li>Hospitals</li>
                          <li>Public Service</li>
                          <li>Recruitment and HR</li>
                          <li>Social Care</li>
                          <li>Security</li>
                          <li>Transport and Logistics</li>
                          <li>Delivery</li>
                          <li>Sales</li>  
                          <li>Hospitality</li>
                          <li>Service Sector</li> 
                          </ul>  -->
                
                                <div class="details-btn text-center">
                                <a href="#" class="btn btn-sm btn-trastaven">VIEW DETAILS</a>
                             
                                </div>
                                
                        </div>
                      </div>
                    </div> 
                   <!--card  -->
      </div>
      <!-- /.col -->
   </div>
   <!-- /.row -->

</div>
<!-- /.container-fluid -->

























@endsection