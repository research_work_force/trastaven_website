@extends('trastaven.frontend.layouts.contactlayout')

@section('content')

<div class="container-fluid mt-1">
    <div class="row services">
      <div class="col-sm-12 col-md-6">
       <div class="contact-form">
       
       <!-- contact forms heading -->
         <h3>Contact Us</h3>

         <!-- contact us forms -->
         <form action="/save/contact" method="post">
          {{ csrf_field() }}
            <div class="form-group">
            <input type="text" class="form-control" name="name" id="formGroupExampleInput" placeholder="Your Name" required>
            </div>
            <div class="form-group">
            <input type="text" class="form-control" name="ph" id="formGroupExampleInput2" placeholder="Your Phone Number" required>
            </div>
            <div class="form-group">
            <input type="email" class="form-control" name="email" id="formGroupExampleInput2" placeholder="Your Email id" required>
            </div>
            <div class="form-group">
            <input type="text" class="form-control" name="msg" id="formGroupExampleInput2" placeholder="Your Messege" required>
            </div>

       
          <button type="submit" class="btn btn-lg btn-trastaven">Submit</submit>
 </form>
       </div>
      </div>
      <div class="col-sm-12 col-md-6">
      
      </div>
    </div>
</div>



@endsection