@extends('trastaven.frontend.layouts.traininglayout')

@section('content')
         
            <!-- -_-_-_- 8th part -->
            <div class="affliate">


                              

                  <form action="">
                 
                   <div class="form-group">
                        <label for="sel1"><h5 class="my-2">Affliated by</h5></label>
                        <select class="form-control" id="sel1" name="sellist1">
                           
                              <!-- 1st Check box -->

                              <option>
                                    <p>
                                    Mahatma Gandhi Kashi Vidyapith - [MGKV], Varanasi                         
                                          
                                    </p>
                              </option>

                              <!-- 2nd Check box -->

                              <option>
                              Guru Nanak Dev University - [GNDU], Amritsar 
                              </option>


                              <!-- 3rd Check box -->


                              <option>
                              Indira Kala Sangeet Vishwavidyalaya - [IKSVV], Khairagarh
                              </option>

                              <!-- 4th Check box -->

                              <option>
                              University of Delhi - [DU], New Delhi 

                              </option>


                              <!-- 5th Check box -->

                              <option>
                              Savitribai Phule Pune University, Pune 
                              </option>

                              <!-- 6th Check box -->

                              <option>
                              University of Mumbai, Mumbai 
                              </option>


                              <!-- 7th Check box -->


                              <option>
                              Aligarh Muslim University - [AMU], Aligarh
                              </option>


                              <!-- 8th Check box -->

                              <option>
                              Patna University - [PU], Patna 
                              </option>


                              <!-- 9th Check box -->

                              <option>
                              Chaudhary Charan Singh University - [CCSU], Meerut 
                              </option>

                              <!-- 10th Check box -->

                              <option>
                              Rashtrasant Tukadoji Maharaj Nagpur University - [RTMNU], Nagpur 
                              </option>

                              <!-- 11th Check box -->

                              <option>
                              University of Lucknow, Lucknow
                              </option>


                              <!-- 12th Check box -->

                              <option>
                              Kumaun University, Nainital 
                              </option>

                              <!-- 13th Check box -->

                              <option>
                              Swami Vivekanand Subharti University- [SVSU], Meerut 
                              </option>


                              <!-- 14th Check box -->


                              <option>
                              Annamalai University, Cuddalore 
                              </option>

                              <!-- 15th Check box -->

                              <option>
                              Uttar Pradesh Technical University, Lucknow 
                              </option>


                              <!-- 16th Check box -->

                              <option>
                              Dr Hari Singh Gour University, Sagar 
                              </option>

                              <!-- 17th Check box -->

                              <option>
                              Banaras Hindu University - [BHU], Varanasi 
                              </option>


                              <!-- 18th Check box -->


                              <option>
                              Symbiosis International University - [SIU], Pune 
                              </option>

                              <!-- 19th Check box -->

                              <option>
                              Uttarakhand Technical University, Dehradun 

                              </option>

                              <!-- 20th Check box -->

                              <option>
                              Dr. A.P.J. Abdul Kalam Technical University - [APJAKTU], Lucknow 

                              </option>


                              <!-- 21st Check box -->

                              <option>
                              Mahatma Gandhi University - [MGU], Kottayam 
                              </option>

                              <!-- 22nd Check box -->

                              <option>
                              Bangalore University - [BU], Bangalore 
                              </option>


                              <!-- 23rd Check box -->


                              <option>
                              Teerthanker Mahaveer University - [TMU], Moradabad 
                              </option>

                              <!-- 24th Check box -->

                              <option>
                              Veer Narmad South Gujarat University - [VNSGU], Surat 

                              </option>


                              <!-- 25th Check box -->

                              <option>
                              Goa University, North Goa 
                              </option>

                              <!-- 26th Check box -->

                              <option>
                              Mahatma Gandhi University, Nalgonda 
                              </option>


                              <!-- 27th Check box -->


                              <option>
                              Bharathidasan University - [BU], Thiruchirapalli 
                              </option>


                              <!-- 28th Check box -->

                              <option>
                              Patna University - [PU], Tumkur University, Tumkur Patna 
                              </option>


                              <!-- 29th Check box -->

                              <option>
                              Vinayaka Missions University - [VMU], Salem 
                              </option>

                              <!-- 30th Check box -->

                              <option>
                              NIMS University, Jaipur 
                              </option>

                              <!-- 31th Check box -->

                              <option>
                              Bharati Vidyapeeth Deemed University - [BVDU], Pune
                              </option>


                              <!-- 32th Check box -->

                              <option>
                              University of Burdwan, Bardhaman 
                              </option>

                              <!-- 33th Check box -->

                              <option>
                              I.K. Gujral Punjab Technical University - [PTU], Jalandhar
                              </option>


                              <!-- 34th Check box -->


                              <option>
                              Osmania University - [OU], Hyderabad
                              </option>

                              <!-- 35th Check box -->

                              <option>
                              Allahabad University, Allahabad
                              </option>


                              <!-- 36th Check box -->

                              <option>
                              Bundelkhand University, Jhansi 
                              </option>

                              <!-- 37th Check box -->

                              <option>
                              University of Calicut, Calicut 
                              </option>


                              <!-- 38th Check box -->


                              <option>
                              Alliance University - [AU], Bangalore 
                              </option>

                              <!-- 39th Check box -->

                              <option>
                              Jamia Millia Islamia University-[JMI], New Delhi

                              </option>

                              <!-- 40th Check box -->

                              <option>
                              Amity University, Jaipur 

                              </option>


                              <!-- 41st Check box -->

                              <option>
                              Chhatrapati Sahu Ji Maharaj University - [CSJMU], Kanpur
                              </option>


                              <!-- 42nd Check box -->

                              <option>
                              University of Kerala, Thiruvananthapuram 
                              </option>


                              <!-- 43rd Check box -->


                              <option>
                              Jawaharlal Nehru Architecture and Fine Arts University - [JNAFA], Hyderabad
                              </option>

                              <!-- 44th Check box -->

                              <option>
                              Amity University, Lucknow 

                              </option>


                              <!-- 45th Check box -->

                              <option>
                              Desh Bhagat University - [DBU], Gobindgarh 
                              </option>

                              <!-- 46th Check box -->

                              <option>
                              Kannada University - [KU], Hampi 
                              </option>


                              <!-- 47th Check box -->


                              <option>
                              Solapur University, Solapur 
                              </option>


                              <!-- 48th Check box -->

                              <option>
                              Amity University, Noida 
                              </option>


                              <!-- 49th Check box -->

                              <option>
                              Dr Babasaheb Ambedkar Marathwada University, Aurangabad
                              </option>

                              <!-- 50th Check box -->

                              <option>
                              KLE University, Belgaum 
                              </option>

                              <!-- 51th Check box -->

                              <option>
                              Sri Venkateswara University - [SVU], Tirupati 
                              </option>


                              <!-- 52th Check box -->

                              <option>
                              Andhra University - [AU], Visakhapatnam 

                              </option>

                              <!-- 53th Check box -->

                              <option>
                              Dr Bhim Rao Ambedkar University - [DBRAU], Agra
                              </option>


                              <!-- 54th Check box -->


                              <option>
                              University of Mysore - [UOM], Mysore 
                              </option>

                      </select>
                   </div>

                  
                        
                  </form>

                              
            </div>
            <!--/.affliated -->
@endsection