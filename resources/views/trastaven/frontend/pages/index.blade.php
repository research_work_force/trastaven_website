@extends('trastaven.frontend.layouts.homepage')

@section('content')

<!-- data-interval="3000"  -->
<div class="banner">
               <div class="carousel slide" data-ride = "carousel" id="changable">
                     <div class="carousel-inner">
                           <div class="carousel-item active">
                             <img src="{{ asset('trastaven/frontend/images/a.png') }}" alt="hero">
                             <div class="carousel-caption">
                                  <h1 class="trastaven-carousel-heading">Worried about<br> #Education? </h1>
                                  <h4 class="trastaven-carousel-body d-xs-none d-md-block"> We provide one stop solution for all your educational needs. <br>You can trust us, because we are <span class="trastaven-name-banner">trastaven.</span></h4>
                                  <!-- <a href="#" class="btn btn-trastavenbanner"> Get Admission</a>   -->

                             </div>   

                           </div>
                           <!-- <div class="carousel-item">
                                  <img src="images/hero2.jpg" alt="hero">
                                  <div class="carousel-caption">
                                       <h1>A fight that guarentees a job</h1>
                                       <p> Lorem ipsum dolor sit amet consectetur, adipisicing elit. Saepe quos sapiente facere ipsam nobis officia. Ullam sint</p>
                                  </div>      
                           </div>
                           <div class="carousel-item">
                                  <img src="images/sign-up.jpg" alt="hero">
                                  <div class="carousel-caption">
                                       <h1>A tour that guarentees a job</h1>
                                       <p> Lorem ipsum dolor sit amet consectetur, adipisicing elit. Saepe quos sapiente facere ipsam nobis officia. Ullam sint</p>
                                  </div>      
                            </div> -->
                            
                     </div>
                     
                     <div class="d-lg-flex flex-row big-button">
                      <div class="">
                        <a href="/filter/courses" class="btn btn-trastaven mb-3 mr-2" style="font-weight: bold;"> <h4>Get Admission</h4> </a>
                      </div>
                      <div class="  ">
                        <a href="/contact" class="btn btn-trastaven" style="background: white;"> <h4 style="color: #00b5ac;font-weight: bold;">Contact Now</h4></a>
                      </div>  
                           
                           
                        
                     </div>
                     <!-- carousel indicators -->
                     <!-- <ul class="carousel-indicators">
                           <li data-target="#changable" data-slide-to="0" class="active"></li>
                           <li data-target="#changable" data-slide-to="1"></li>
                           <li data-target="#changable" data-slide-to="2"></li>
                     </ul> -->
               </div> 
            </div> 
     
            
          <!--Services  -->
         <!-- Services -->

            <!-- ***** 4 in one column ***** -->
            <div class="container-fluid services text-center">
                  <div class="row">
                        <!-- 1st service -->
                        <div class="col-md-3">
                              <div class="card trastaven-card">
                                    <img src="{{ asset('trastaven/frontend/images/icons/admissionindia.png') }}" alt="">
                                    <div class="card-body">
                                          <h4 class="trastaven-course-heading">Admission India</h4>
                                           <h6 class="trastaven-course-body text-secondary services-h6">Trastaven is ready to help you to get a better education and guide you to get admission in top colleges in India, whatever your stream is.
                                           </h6>
                                           <a href="#" class="btn btn-trastaven"> Get Admission</a>


                                    </div>
                              </div>
                              
                        </div>
                        <!-- 2nd service -->
                        <div class="col-md-3">
                              <div class="card trastaven-card">
                                    <img src="{{ asset('trastaven/frontend/images/icons/admissionworld.png') }}" alt="">
                                    <div class="card-body">
                                          <h4 class="trastaven-course-heading">Admission Abroad</h4>
                                           <h6 class="trastaven-course-body text-secondary services-h6">If you want to get admission in abroad in future, Join with Trastaven. It is great opportunity to make your dream come true regarding education to study outside of India. 
                                           </h6>
                                           <a href="#" class="btn btn-trastaven"> Get Admission</a>


                                    </div>
                              </div>
                              
                        </div>
                        <!-- 3rd service -->
                        <div class="col-md-3">
                              <div class="card trastaven-card">
                                    <img src="{{ asset('trastaven/frontend/images/icons/placement.png') }}" alt="">
                                    <div class="card-body">
                                                <h4 class="trastaven-course-heading">Placement Support</h4>
                                           <h6 class="trastaven-course-body text-secondary services-h6">Not only Trastaven will provide you with a better admission, better education, also will look for your placement. We will be most happy after you get a job in India or outside of the country. 
                                           </h6>
                                                <a href="#" class="btn btn-trastaven"> Enroll Now</a>


                                    </div>
                              </div>
                                    
                              </div>
                        <!-- 4rth service -->
                        <div class="col-md-3">
                              <div class="card trastaven-card">
                                    <img src="{{ asset('trastaven/frontend/images/icons/training.png') }}" alt="">
                                    <div class="card-body">
                                                <h4 class="trastaven-course-heading">Training</h4>
                                           <h6 class="trastaven-course-body text-secondary services-h6">regarding your education, Trastaven is ready to train you, ready to make You professional and practical to your work field. Get here admission and we are going to give you the finest training across India or other largest countries. </h6>
                                                <a href="#" class="btn btn-trastaven"> Enquiry Now</a>


                                    </div>
                              </div>
                              
                        </div>
                  </div>
               </div> 
               <!-- trending courses  -->
<!-- 
               <div class="container-fluid trending-courses">
                        <section class="container p-t-3">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <h1>Trending Courses</h1>
                                        </div>
                                    </div>
                                </section>
                                <div class="carousel slide" data-ride="carousel" id="postsCarousel">
                              
                                    <div class="container-fluid p-t-0 carousel-inner">
                                        <div class="row row-equal carousel-item active m-t-0">
                                            <div class="col-md-4">
                                                <div class="card trastaven-card">
                                                    <div class="card-img-top card-img-top-250">
                                                            <img class="img-fluid" src="//visualhunt.com/photos/l/1/office-student-work-study.jpg" alt="Carousel 4">
                                                    </div>
                                                    <div class="card-block p-2">
                                                            <h6>This is heading</h6>
                                                            <p><strong><i class="fas fa-map-marker-alt pr-2"></i>Location</strong> </p>
                                                            <a href="#" class="btn"> Details</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="card trastaven-card">
                                                    <div class="card-img-top card-img-top-250">
                                                            <img class="img-fluid" src="//visualhunt.com/photos/l/1/office-student-work-study.jpg" alt="Carousel 4">
                                                    </div>
                                                    <div class="card-block p-2">
                                                            <h6>This is heading</h6>
                                                            <p><strong><i class="fas fa-map-marker-alt pr-2"></i>Location</strong> </p>
                                                            <a href="#" class="btn"> Details</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="card trastaven-card">
                                                    <div class="card-img-top card-img-top-250">
                                                            <img class="img-fluid" src="//visualhunt.com/photos/l/1/office-student-work-study.jpg" alt="Carousel 4">
                                                    </div>
                                                    <div class="card-block p-2">
                                                            <h6>This is heading</h6>
                                                            <p><strong><i class="fas fa-map-marker-alt pr-2"></i>Location</strong> </p>
                                                            <a href="#" class="btn"> Details</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row row-equal carousel-item m-t-0">
                                            <div class="col-md-4">
                                                <div class="card trastaven-card">
                                                    <div class="card-img-top card-img-top-250">
                                                        <img class="img-fluid" src="//visualhunt.com/photos/l/1/office-student-work-study.jpg" alt="Carousel 4">
                                                    </div>
                                                    <div class="card-block p-2">
                                                            <h6>This is heading</h6>
                                                            <p><strong><i class="fas fa-map-marker-alt pr-2"></i>Location</strong> </p>
                                                            <a href="#" class="btn"> Details</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="card trastaven-card">
                                                    <div class="card-img-top card-img-top-250">
                                                            <img class="img-fluid" src="//visualhunt.com/photos/l/1/office-student-work-study.jpg" alt="Carousel 4">
                                                    </div>
                                                    <div class="card-block p-2">
                                                            <h6>This is heading</h6>
                                                            <p><strong><i class="fas fa-map-marker-alt pr-2"></i>Location</strong> </p>
                                                            <a href="#" class="btn"> Details</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4 fadeIn wow">
                                                <div class="card trastaven-card">
                                                    <div class="card-img-top card-img-top-250">
                                                            <img class="img-fluid" src="//visualhunt.com/photos/l/1/office-student-work-study.jpg" alt="Carousel 4">
                                                    </div>
                                                    <div class="card-block p-2">
                                                            <h6>This is heading</h6>
                                                            <p><strong><i class="fas fa-map-marker-alt pr-2"></i>Location</strong> </p>
                                                            <a href="#" class="btn"> Details</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                              
                                    <div class="container-fluid mt-4">
                                        <div class="d-flex d-flex-row slider-button justify-content-end">
                                                <a class="btn btn-outline-secondary prev" href="" title="go back"><i class="fa fa-lg fa-chevron-left"></i></a>
                                                <a class="btn btn-outline-secondary next" href="" title="more"><i class="fa fa-lg fa-chevron-right"></i></a>
                                        </div>
                                    </div>
                                </div>
            </div> -->

<!-- Sign Up page -->
       <div class="container-fluid sign-up">
            <div class="row content">
                  <div class="col-sm-12 why mt-3 col-md-6 text-left text-white">
                  <h1>
                        Why Choose Trastaven?
                  </h1>
                  <hr>
                  <br>
                  <p class="why-us">
                  If you are aspiring for a rewarding career in future and if you think a world-class study program is quintessential for the success of your career plans, then trust Trastaven - the pioneers in overseas education counselling - to equip you with the best possible academic opportunities in respected universities abroad.This explains why our services are limited to English-speaking regions only. Get a admission in trastaven.
                  Trastaven with a competitive edge and make it your ideal partner in your academic pursuits that will play a vital role in shaping your career ahead in india or outside of the country:

                  </p>
                        <ul>
                              <li>Expertise in the overseas education counselling domain</li>
                              <li>India’s top-rated education consultancy</li>
                              <li>A competent team of experienced and friendly counsellors</li>
                              <li>A student-centred strategy that emphasizes on personalised attention to each student</li>
                              <li>Meticulous evaluation of students on various parameters</li>
                              <li>Trusted partner for many students.</li>
                        </ul>
                  
                     <div class="know-button">
                        <a href="/contact" class="btn btn-trastaven btn-lg text-white">Know More</a>
                   </div>

                  </div>      
                  
                  <div class="col-sm-12 col-md-6 col-xl-6">
                  <div class="card rounded-0">
                        
                        <div class="card-title text-center text-white mt-3 mb-2">Explore Your Study Option</div>
                          
                        <div class="card-body text-center">
                          <form action="" class="text-center">
                              <input type="text" class="form-control" placeholder="Your Name">
                              <input type="number" class="form-control"  placeholder="Your Phone Number">
                              <input type="text" class="form-control"  placeholder="Your Email id">
                              <input type="text"  class="form-control"  placeholder="Your Preffered Location">
                              <input type="text" class="form-control"  placeholder="Your Level of Education">
                          </form>
                          <div class="send-button">
                              <a href="#" class="btn btn-trastaven  text-white">Send</a>
                         </div>
                        </div>
                        
                  </div>
                  </div>
            </div> 
       </div>
      <!-- Events -->
        <!--  <div class="container-fluid  events">
               <div class="row">
                <div class="col-sm-12 col-md-6">
                  <h1 class="eve-heading">
                    Upcoming Events 
                  </h1>
                  <hr>
                  <br>
                  <h4>MEDICAL COUNSELLING</h4>
                  <p >
                  Exclusive counselling session for students looking for medical education in Bangladesh in 2019. An open discussion on the leading colleges, admission procedure, fees structure, culture orientation, etc. Attend the session to know A-Z about studying MBBS in Bangladesh
                  </p>
                  
                  <a href="#">Read More</a>
                  
                  <h4>DISCOUNT OFFER</h4>
                  <p>
                  Unique chance to avail up-to 50% discount on any course offered by any of our vocational training institute at Howrah, Rishra and Uluberia. Choose from a wide array of courses like computer fundamentals, multimedia, hardware & networking, software development, etc. Get ready for the corporate world! Offer valid till 30.04.2019. T&C Apply.
                  </p>
                 
                  <a href="#">Read More</a>
                
                  <h4>INFINITY CREATIVE ART</h4>
                  <p>
                  Our latest venture, Infinity Creative Art Academy (ICAA) is inviting aspiring minds interested to explore the world of art, painting, drawing, sculpting, etc. Interactive sessions taken by eminent artists, different forms of painting, exhibition, workshop and many more opportunities to nurture the artist inside you.

                 </p>
                  <a href="#">Read More</a>
                 
                  
                </div>
               
                  <div class="col-sm-12 col-lg-6">
                        <h1 class="eve-heading" >
                        Whats New 
                        </h1>   
                        <hr>
                     <div class="card eve">
                       <div class="card-body">
                             <img src="{{ asset('trastaven/frontend/images/event.jpg') }}" alt="">
                             <br>
                             <p class="text-center">
                              Infinity is organizing VOYAGE ADMISSION FAIR 2019 in the month of June 2019 with over 50+ medical and technical institutions at Rotary Sadan, Kolkata. Come witness the direct participation of several foreign medical colleges from Nepal, China, Bangladesh, Philippines, Poland, Russia, etc.
                             </p>
                             <div class="big-button text-center">
                              <a href="#" class="btn btn-lg btn-trastaven text-white text-center">Know More</a>
                         </div>
                       </div>
                     </div>   
                  </div>
               </div>
         </div>
         --> 
         <!-- Our Students -->
         <!-- <div class="container-fluid students ">
            <h1>Our Students</h1>
            <hr>
            <div class="row">
                  <div class="col-sm-6 col-md-3">
                     <div class="card trastaven-card">
                           <div class="card-body">
                               <img src="images/user1.jpg" alt="">

                           </div>
                           <div class="bio text-center">
                                   <h1> Gargi Ghorai</h1>
                                    <p>(BANGLADESH)</p> 
                           </div>
                     </div>
                  </div>
                  <div class="col-sm-6 col-md-3">
                  <div class="card trastaven-card">
                        <div class="card-body">
                              <img src="images/user2.png" alt="">

                        </div>
                        <div class="bio text-center">
                                    <h1> Ashiyana Ahmed</h1>
                                    <p>(BANGLADESH)</p> 
                        </div>
                  </div>
                  </div>
                  <div class="col-sm-6 col-md-3">
                  <div class="card trastaven-card">
                        <div class="card-body">
                              <img src="images/user3.jpg" alt="">

                        </div>
                        <div class="bio text-center">
                                    <h1> Gargi Ghorai</h1>
                                    <p>(BANGLADESH)</p> 
                        </div>
                  </div>
                  </div>
                  <div class="col-sm-6 col-md-3">
                  <div class="card trastaven-card">
                        <div class="card-body">
                              <img src="images/user4.jpg" alt="">

                        </div>
                        <div class="bio text-center">
                                    <h1> Gargi Ghorai</h1>
                                    <p>(BANGLADESH)</p> 
                        </div>
                  </div>
                  </div>
                  
            </div>
            <br>
            <div class="row second">
                        <div class="col-sm-6 col-md-3">
                           <div class="card trastaven-card">
                                 <div class="card-body">
                                     <img src="images/user5.jpg" alt="">
      
                                 </div>
                                 <div class="bio text-center">
                                         <h1> Gargi Ghorai</h1>
                                          <p>(BANGLADESH)</p> 
                                 </div>
                           </div>
                        </div>
                        <div class="col-sm-6 col-md-3">
                        <div class="card trastaven-card">
                              <div class="card-body">
                                    <img src="images/user8.png" alt="">
      
                              </div>
                              <div class="bio text-center">
                                          <h1> Ashiyana Ahmed</h1>
                                          <p>(BANGLADESH)</p> 
                              </div>
                        </div>
                        </div>
                        <div class="col-sm-6 col-md-3">
                        <div class="card trastaven-card">
                              <div class="card-body">
                                    <img src="images/user6.jpg" alt="">
      
                              </div>
                              <div class="bio text-center">
                                          <h1> Gargi Ghorai</h1>
                                          <p>(BANGLADESH)</p> 
                              </div>
                        </div>
                        </div>
                        <div class="col-sm-6 col-md-3">
                        <div class="card trastaven-card">
                              <div class="card-body">
                                    <img src="images/user7.jpg" alt="">
      
                              </div>
                              <div class="bio text-center">
                                          <h1> Gargi Ghorai</h1>
                                          <p>(BANGLADESH)</p> 
                              </div>
                        </div>
                        </div>
                        
                  </div>
         </div> -->
      




@endsection