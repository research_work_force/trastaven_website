@extends('trastaven.frontend.layouts.traininglayout')

@section('content')

<div class="container-fluid mt-3">
   <div class="row services">
      <div class="col-md-6 col-sm-12">
      <div class="card" id ="think-card">
                      
                      <div class="card-body  text-center">
                        
                              <img src="{{asset('trastaven/frontend/images/icons/admissionindia.png')}}"  class="img-fluid" alt="Responsive image">
                    
                           <div class="card-heading">
                           <h5>General</h5>
                           <!-- <hr class="think-hr-sky"> -->
                           </div>

                        <div class="training-details">
                                <h6>SSC, RRB, SSB, Bank, UPSC, TET Etc.</h6>
                                <p class="text-left"> Trastaven will prepare you for SSC, RRB, SSB, BANK, UPSC, TET  Etc exam to score a great rank. Get admission as per your educational need and be prepared to crack these exams with us.  

                                </p>
                                <div class="details-btn text-center">
                                <div class="details-btn text-center">
                                <a href="#" class="btn btn-sm btn-trastaven">VIEW DETAILS</a>
                                </div>
                                </div>
                        </div>
                      </div>
                    </div> 
                   <!--card  -->
      </div>
      <!-- /.col -->

      <div class="col-md-6 col-sm-12">
      <div class="card" id ="think-card">
                      <div class="card-body text-center">
                        
                              <img src="{{asset('trastaven/frontend/images/icons/admissionindia.png')}}" class="img-fluid" alt="Responsive image">
                    
                           <div class="card-heading">
                           <h5>Distance Learning Programme</h5>
                           <!-- <hr class="think-hr-sky"> -->
                           </div>
                        <div class="training-details">
                                <h6>SSC, RRB, SSB, Bank, UPSC, TET Etc.</h6>
                                <p class="text-left"> Trastaven will prepare you for SSC, RRB, SSB, BANK, UPSC, TET  Etc exam to score a great rank. Get admission as per your educational need and be prepared to crack these exams with us.  

                                </p>
                                <div class="details-btn text-center">
                                <div class="details-btn text-center">
                                <a href="#" class="btn btn-sm btn-trastaven">VIEW DETAILS</a>
                                </div>
                                </div>
                                
                        </div>
                      </div>
                    </div> 
                   <!--card  -->
      </div>
      <!-- /.col -->
   </div>
   <!-- /.row -->

</div>
<!-- /.container-fluid -->

<div class="container-fluid trastaven-abroad mt-3">
 <!-- training heading -->
<!--  
 <div class="row think-heading">
 <h3 class= "">General</h3>
 </div>
  -->
 
 
 

 <!-- sub parts -->

<!-- ---------- SSC ---------- -->
 <div class="row services"  id ="placement">
               <div class="col-sm-12 col-lg-4 ">
                    <div class="card" id ="think-card">
                      <div class="card-body text-center">
                        
                              <img src="{{asset('trastaven/frontend/images/icons/admissionindia.png')}}" alt="" class  ="img-responsive" >
                    
                        <div class="training-details">
                                <h6>Skill Development</h6>
                                <p class="text-left"> Trastaven will develop your skills. Not only your working skills, but it will also help to develop your soft skills. We will make you ready to put the effort into the work field. 

                                </p>
                                <div class="details-btn text-center">
                                <a href="#" class="btn btn-sm btn-trastaven">VIEW DETAILS</a>
                                </div>
                        </div>
                      </div>
                    </div> 
                   <!--card  -->
                </div>
                <!-- /.col -->
                <div class="col-sm-12 col-lg-4">
                <div class="card" id ="think-card">
                      <div class="card-body text-center">
                        
                              <img src="{{asset('trastaven/frontend/images/icons/admissionindia.png')}}" alt="" class  ="img-responsive" >
                    
                        <div class="training-details">
                                   <h6>Groomimg</h6>
                                    <p class="text-left">Trastaven will groom you also.we will short out your weakness, and make you strong enough to face any kind of problem regarding work. Get admission to be smarter.</p>
                                <div class="details-btn text-center">
                                <a href="#" class="btn btn-sm btn-trastaven">VIEW DETAILS</a>
                                </div>
                        </div>
                      </div>
                    </div> 
                   <!--card  --> 
                </div> 
                <!-- /.col -->
                <div class="col-sm-12 col-lg-4">
                <div class="card" id ="think-card">
                      <div class="card-body text-center">
                        
                              <img src="{{asset('trastaven/frontend/images/icons/admissionindia.png')}}" alt="" class  ="img-responsive" >
                    
                        <div class="training-details">
                                   <h6>Parental Grooming </h6>
                                    <p class="text-left">Trastaven care’s for the parents also. That's why we will groom your parents an short out their problems and weaknesses regarding your studies.</p>
                                <div class="details-btn text-center">
                                <a href="#" class="btn btn-sm btn-trastaven">VIEW DETAILS</a>
                                </div>
                        </div>
                      </div>
                    </div> 
                   <!--card  -->
                </div> 
              <!-- /.col  -->
           
</div>
<!-- /.row -->

<!-- New Row -->
<div class="row services"  id ="placement">
               <div class="col-sm-12 col-lg-4">
                    <div class="card" id ="think-card">
                      <div class="card-body text-center">
                        
                              <img src="{{asset('trastaven/frontend/images/icons/admissionindia.png')}}" alt="" class  ="img-responsive" >
                    
                        <div class="training-details">
                                   <h6>Industrial Training </h6>
                                    <p class="text-left">Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusantium voluptatibus ipsum enim! Atque magnam, dicta officia aut impedit distinctio totam?</p>
                                <div class="details-btn text-center">
                                <a href="#" class="btn btn-sm btn-trastaven">VIEW DETAILS</a>
                                </div>
                        </div>
                      </div>
                    </div> 
                   <!--card  -->
                </div>
                <!-- /.col -->
                <div class="col-sm-12 col-lg-4">
                <div class="card" id ="think-card">
                      <div class="card-body text-center">
                        
                              <img src="{{asset('trastaven/frontend/images/icons/admissionindia.png')}}" alt="" class  ="img-responsive" >
                    
                        <div class="training-details">
                                <h6>Managemental</h6>
                                <p class="text-left"> Lorem, ipsum dolor sit amet consectetur adipisicing elit. Est totam, incidunt asperiores blanditiis numquam debitis qui doloribus hic praesentium voluptatem.
                                </p>
                                <div class="details-btn text-center">
                                <a href="#" class="btn btn-sm btn-trastaven">VIEW DETAILS</a>
                                </div>
                        </div>
                      </div>
                    </div> 
                   <!--card  --> 
                </div> 
                <!-- /.col -->
                <div class="col-sm-12 col-lg-4">
                <div class="card" id ="think-card">
                      <div class="card-body text-center">
                        
                              <img src="{{asset('trastaven/frontend/images/icons/admissionindia.png')}}" alt="" class  ="img-responsive" >
                    
                        <div class="training-details">
                                <h6>Communication Technical Training</h6>
                                <p class="text-left">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Sit vero est provident accusantium repellendus soluta. Obcaecati numquam vero error assumenda.
                                </p>
                                <div class="details-btn text-center">
                                <a href="#" class="btn btn-sm btn-trastaven">VIEW DETAILS</a>
                                </div>
                        </div>
                      </div>
                    </div> 
                   <!--card  -->
                </div> 
              <!-- /.col  -->
           
</div>
<!-- /.row -->


</div>
<!-- /.container-fluid -->

<!-- Dlp module -->

<!-- <div class="container-fluid"> -->
<!-- heading pan india -->
<!-- <h1>DLP Module</h1> -->

<!-- sub parts -->

<!-- ssc -->
<!-- <div class="row industrial-sector">
               <div class="col-sm-12 col-lg-6">
                        <div class="training-img right">
                              <img src="{{asset('trastaven/frontend/images/icons/admissionindia.png')}}" alt="">
  
                        </div>
                </div>
                <div class="col-sm-12 col-lg-6">
                        <div class="training-details">
                                    <h3>SSC, RRB, SSB, Bank, UPSC, TET Etc.</h3>
                                    <p class="text-left"> Trastaven will prepare you for SSC, RRB, SSB, BANK, UPSC, TET  Etc exam to score a great rank. Get admission as per your educational need and be prepared to crack these exams with us.  

                                    </p>
                                    <a href="#" class="btn btn-trastaven">VIEW DETAILS</a>
        
                        </div>
                </div>      
           
</div> -->

<!-- ---------   skill development  ------------ -->

<!-- <div class="row skill-development">
               
                <div class="col-sm-12 col-lg-6">
                        <div class="training-details">
                                    <h3>Skill Development</h3>
                                    <p class="text-left">Trastaven will develop your skills. Not only your working skills, but it will also help to develop your soft skills. We will make you ready to put the effort into the work field. 

                                    </p>
                                    <a href="#" class="btn btn-trastaven">VIEW DETAILS</a>
        
                        </div>
                </div> 
                <div class="col-sm-12 col-lg-6">
                        <div class="training-img right">
                              <img src="{{asset('trastaven/frontend/images/icons/admissionindia.png')}}" alt="">
  
                        </div>
                </div>     
           
</div> -->

<!-- ---------  Groomimg ------------ -->
<!-- 
<div class="row groomimg">
                    
                <div class="col-sm-12 col-lg-6">
                        <div class="training-img right">
                              <img src="{{asset('trastaven/frontend/images/icons/admissionindia.png')}}" alt="">
  
                        </div>
                </div>
                <div class="col-sm-12 col-lg-6">
                        <div class="training-details">
                                    <h3>Groomimg</h3>
                                    <p class="text-left">Trastaven will groom you also.we will short out your weakness, and make you strong enough to face any kind of problem regarding work. Get admission to be smarter.</p>
                                    <a href="#" class="btn btn-trastaven">VIEW DETAILS</a>
        
                        </div>
                </div> 
           
</div> -->



<!-- ---------   Parental grooming  ------------ -->

<!-- <div class="row parental-grooming ">
               
                <div class="col-sm-12 col-lg-6">
                        <div class="training-details">
                                    <h3>Parental Grooming </h3>
                                    <p class="text-left">Trastaven care’s for the parents also. That's why we will groom your parents an short out their problems and weaknesses regarding your studies.</p>
                                    <a href="#" class="btn btn-trastaven">VIEW DETAILS</a>
        
                        </div>
                </div> 
                <div class="col-sm-12 col-lg-6">
                        <div class="training-img right">
                              <img src="{{asset('trastaven/frontend/images/icons/admissionindia.png')}}" alt="">
  
                        </div>
                </div>     
           
</div> -->

<!-- ---------  Industrial Training ------------ -->

<!-- <div class="row industrial-training ">
                    
                <div class="col-sm-12 col-lg-6">
                        <div class="training-img right">
                              <img src="{{asset('trastaven/frontend/images/icons/admissionindia.png')}}" alt="">
  
                        </div>
                </div>
                <div class="col-sm-12 col-lg-6">
                        <div class="training-details">
                                    <h3>Industrial Training </h3>
                                    <p class="text-left">Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusantium voluptatibus ipsum enim! Atque magnam, dicta officia aut impedit distinctio totam?</p>
                                    <a href="#" class="btn btn-trastaven">VIEW DETAILS</a>
        
                        </div>
                </div> 
           
</div> -->


<!-- ---------   Managemental  ------------ -->

<!-- <div class="row managemental">
               
                <div class="col-sm-12 col-lg-6">
                        <div class="training-details">
                                    <h3>Managemental</h3>
                                    <p class="text-left">Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusantium voluptatibus ipsum enim! Atque magnam, dicta officia aut impedit distinctio totam?</p>
                                    <a href="#" class="btn btn-trastaven">VIEW DETAILS</a>
        
                        </div>
                </div> 
                <div class="col-sm-12 col-lg-6">
                        <div class="training-img right">
                              <img src="{{asset('trastaven/frontend/images/icons/admissionindia.png')}}" alt="">
  
                        </div>
                </div>     
           
</div> -->

<!-- ---------  Communication ------------ -->

<!-- <div class="row communication">
                    
                <div class="col-sm-12 col-lg-6">
                        <div class="training-img right">
                              <img src="{{asset('trastaven/frontend/images/icons/admissionindia.png')}}" alt="">
  
                        </div>
                </div>
                <div class="col-sm-12 col-lg-6">
                        <div class="training-details">
                                    <h3>Communication Technical Training</h3>
                                    <p class="text-left">Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusantium voluptatibus ipsum enim! Atque magnam, dicta officia aut impedit distinctio totam?</p>
                                    <a href="#" class="btn btn-trastaven">VIEW DETAILS</a>
        
                        </div>
                </div> 
           
</div> -->

<!-- ---------   Nursing  ------------ -->

<!-- <div class="row nursing">
               
                <div class="col-sm-12 col-lg-6">
                        <div class="training-details">
                                    <h3>Nursing Para Medical</h3>
                                    <p class="text-left">Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusantium voluptatibus ipsum enim! Atque magnam, dicta officia aut impedit distinctio totam?</p>
                                    <a href="#" class="btn btn-trastaven">VIEW DETAILS</a>
        
                        </div>
                </div> 
                <div class="col-sm-12 col-lg-6">
                        <div class="training-img right">
                              <img src="{{asset('trastaven/frontend/images/icons/admissionindia.png')}}" alt="">
  
                        </div>
                </div>     
           
</div> -->


<!-- 5 to 10 all subjects -->
<!-- <div class="container-fluid">
  <h1>Home Tutorial</h1> -->
  
<!-- ---------  5 to 10 ------------ -->

<!-- <div class="row to-ten"> -->
                    
                    <!-- <div class="col-sm-12 col-lg-6">
                            <div class="training-img right">
                                  <img src="{{asset('trastaven/frontend/images/icons/admissionindia.png')}}" alt="">
      
                            </div>
                    </div>
                    <div class="col-sm-12 col-lg-6">
                            <div class="training-details">
                                        <h3>5 to 10 All subjects</h3>
                                        <p class="text-left">Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusantium voluptatibus ipsum enim! Atque magnam, dicta officia aut impedit distinctio totam?</p>
                                        <a href="#" class="btn btn-trastaven">VIEW DETAILS</a>
            
                            </div>
                    </div> 
               
    </div> -->
    
    <!-- ---------   after 10  ------------ -->
<!--     
    <div class="row after ten">
                   
                    <div class="col-sm-12 col-lg-6">
                            <div class="training-details">
                                        <h3>Class after 10 to 12</h3>
                                        <p class="text-left">Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusantium voluptatibus ipsum enim! Atque magnam, dicta officia aut impedit distinctio totam?</p>
                                        <a href="#" class="btn btn-trastaven">VIEW DETAILS</a>
            
                            </div>
                    </div> 
                    <div class="col-sm-12 col-lg-6">
                            <div class="training-img right">
                                  <img src="{{asset('trastaven/frontend/images/icons/admissionindia.png')}}" alt="">
      
                            </div>
                    </div>     
               


</div>
</div> -->



@endsection