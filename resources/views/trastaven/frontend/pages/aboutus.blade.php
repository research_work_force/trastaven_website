@extends('trastaven.frontend.layouts.aboutuslayout')

@section('content')

<!-- hero section -->
<div class="container-fluid">
     <div class="container">
         <h3 class = "text-center">About us</h3>
         <p class = "text-center"></p>
     </div>
     <div class="container hero-image">
       <!-- <img src="{{ asset('trastaven/frontend/images/aaa.png') }}" alt=""> -->
     </div>

</div>

<!-- grids -->

<div class="container">
<!-- 1st grid -->
  <div class="row">
     <div class="col-sm-12 col-md-12">
         <p>Trastaven, incepted in 2019, is a Kerala-based Overseas Educational Consultancy, dedicated to providing best solutions to the Indian students seeking for education in International educational institutions.
Trastaven is one of the Leading Overseas Educational Consultants, acknowledged for providing best guidance and counselling to the students. You can study abroad in best educational hubs of the world, including UK, USA, Australia, Canada, Ireland, and New Zealand with us. As education is the main catalyst in deciding the career of a student, we leave no stone unturned in ensuring that you make the right decision. Our expert mentors understand the dilemma of Indian students and hence, have the par excellence in resolving all their doubts.</p><p>
Trastaven keeps extensive knowledge of ever-changing education sector. With the valuable insights to deliver, we offer the best educational, cultural and financial solutions to the undergraduates, graduates and all the interested students. Our team comprises the potential educators and counselling experts, along with a huge Alumni network to refer you to the top colleges and universities.</p><p>
We provide the complete assistance in Course counselling, University/Country selection, Scholarships, Education Loans, Pre-departure orientations. From form filling to fee submission, from the thought of studying abroad to your departure – we can help you at every step. Trastaven has a right mix of professionals and young education enthusiasts, their exclusive, worthy and ideal knowledge about overseas studies, along with the right tactics to consult the aspirants makes us one of the most sought after Overseas Educational Consultancies in India.
</p>
     
     </div>
   
  </div>

  <!-- 2nd grid -->
  <div class="row">
     <div class="col-sm-12 col-md-4">
     <p>
           
            08420967999
            <br>
            <a href="#"> trastaven@gmail.com</a> 
            <br>
            
            Kolkata
     </p>
     <!-- social icons -->
       <!-- <h6>Connect</h6>
       <div class="d-flex flex-row social-icons">
         <a href="#"><i class="fab fa-twitter-square"></i></a>
         <a href="#"><i class="fab fa-google-plus-square"></i></a>
         <a href="#"><i class="fab fa-facebook-square"></i></a>
         -->
       </div>
     </div>
     <div class="col-sm-12 col-md-8">
     <!-- <img src="http://eighthourday.com/uploads/site/EHD_Process.jpg" alt="Our Process" class="responsive"> -->
     </div>
  </div>

</div>


@endsection