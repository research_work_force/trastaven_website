


      <!-- NAvbar -->
               <nav class="navbar navbar-expand-lg navbar-light  trastaven-navbar">
                       
                     <a class="navbar-brand " href="#"><img src="{{ asset('trastaven/frontend/images/logo/trastaven-logo.jpg') }}" alt="logo"></a>
                     <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                     <span class="navbar-toggler-icon dark"></span>
                     </button>
                     <div class="collapse navbar-collapse" id="navbarNavDropdown">
                     
                     <div class="navbar-nav ml-auto">
                        <a class="nav-item nav-link {{ Request::is('/') ? 'navactive' : '' }} " href="/">Home </a>
                        <a class="nav-item nav-link {{ Request::is('filter/courses') ? 'navactive' : '' }}" href="/filter/courses">Admission </a>
                        <a class="nav-item nav-link {{ Request::is('placement') ? 'navactive' : '' }}" href="/placement">Placement</a>
                        <a class="nav-item nav-link {{ Request::is('training') ? 'navactive' : '' }}" href="/training">Training</a>
                        <a class="nav-item nav-link {{ Request::is('loan') ? 'navactive' : '' }}" href="/loan">EduLoan</a>
                        <a class="nav-item nav-link {{ Request::is('aboutus') ? 'navactive' : '' }}" href="/aboutus">About Us</a>
                        <a class="nav-item nav-link {{ Request::is('contact') ? 'navactive' : '' }}" href="/contact">Contact</a>
                     </div>
                     </div>
                  </nav>
                  