  <div class="col-sm-12 col-lg-3 text-left" id="filter-left">
 
  

                        <a href="#" class="btn btn-trastaven btn-filter my-2 d-md-none d-sm-block">All Filters</a>
                        
        <div class="side-bar"> 
              <div class="d-flex flex-row justify-content-between sticky-top filter-head">
                     <div class="heading">
                        <h3 class="filter my-2 pl-3 ml-1">
                            Filters
                        </h3>

                     </div>
                     <div class="action-button pr-3 mt-3 ">
                        <a href="#"><span class="badge filter-badge"> <h6 class="text-white">Go  <i class="far fa-arrow-alt-circle-right text-white pl-1"></i></h6></span></a>

                     </div>
              </div>

                                      

      <div id="accordion">

      <!-- <div class="card filter-card">
          <h6 class= " mx-3 pl-1 ">Price Range :</h6>
           <span class= " mx-3 pl-1 " id="range"></span>
       
         <div id="slider"></div>
         <div id="output"></div>

      </div> -->
      <div class="card filter-card">
                <div id="headingOne">
                  <h5 class="mb-0">
                    <a class="collapsed"  data-toggle="collapse" data-target="#collapseOnefee" aria-expanded="false" aria-controls="collapseOne">
                      <div class="down-arrow-icon my-2 mx-3 pl-1 d-flex flex-row justify-content-between"> 
                        <div>
                           <h6>Fees Range</h6>
                        </div>
                        <div class="">
                        <i class="fas fa-sort-down"  id="down-arrow" id="down-arrow"></i>

                        </div>  

                      </div>
                    </a>
                  </h5>
                </div>
<form action="/filter/courses" id="filterform" method="post">
   {{ csrf_field() }}
                <div id="collapseOnefee" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                  <div class="card-body">
                          <!-- 1st part -->

                          <div class="course-type">
                                  <!-- 1st part -->
                          @php 
                                $fees = App\Http\Controllers\trastaven\FilterController::FeesRange();
                          @endphp
                                  <!-- 1st Check box -->
                              @foreach ($fees as $data)
                                  <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="fees_range[]" value="{{ $data->id }}" id="defaultCheck1" onchange="call()">
                                    <label class="form-check-label" for="defaultCheck1">
                                      {{ $data->name }}
                                    </label>
                                  </div>
                              @endforeach
                                 
                          </div>
                          <!-- /.course type -->    

                  </div>
                </div>
              </div>

  
              <div class="card filter-card">
                <div id="headingOne">
                  <h5 class="mb-0">
                    <a class="collapsed"  data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                      <div class="down-arrow-icon my-2 mx-3 pl-1 d-flex flex-row justify-content-between"> 
                        <div>
                           <h6>Course  Type </h6>
                        </div>
                        <div class="">
                        <i class="fas fa-sort-down"  id="down-arrow" id="down-arrow"></i>

                        </div>  


                      </div>
                    </a>
                  </h5>
                </div>

                <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                  <div class="card-body">
                          <!-- 1st part -->

                          <div class="course-type">
                                  <!-- 1st part -->
                          @php 
                                $course_type = App\Http\Controllers\trastaven\FilterController::CourseType();
                          @endphp
                                  <!-- 1st Check box -->
                              @foreach ($course_type as $data)
                                  <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="course_type[]" value="{{ $data->id }}" id="defaultCheck1999" onchange="call()">
                                    <label class="form-check-label" for="defaultCheck1">
                                      {{ $data->name }}
                                    </label>

                                  </div>
                              @endforeach
                          
                          </div>
                          <!-- /.course type -->    
                    
                  </div>
                </div>
              </div>

     
              <div class="card filter-card">
                    <div id="headingTwo">
                    <h5 class="mb-0">
                      <a class=" collapsed"  data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                      <div class="down-arrow-icon my-2 mx-3 pl-1 d-flex flex-row justify-content-between"> 
                        <div>
                           <h6>Last Qualification</h6>
                        </div>
                        <div class="">
                        <i class="fas fa-sort-down"  id="down-arrow" id="down-arrow"></i>

                        </div>  
                       </div>
                       <!-- flex -->
                      </a>
                     </h5>
                    </div>

                  <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                    <div class="card-body">


                    
                    <div class="qualification">
                     <!-- <h6 class="my-2">Last Qualification </h6> -->
                      <!-- 1st Check box -->
                      @php 
                            $LastQualification = App\Http\Controllers\trastaven\FilterController::LastQualification();
                          @endphp
                                  <!-- 1st Check box -->
                              @foreach ($LastQualification as $data)
                                  <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="last_qualification[]" value="{{ $data->id }}" id="defaultCheck1" onchange="call()">
                                    <label class="form-check-label" for="defaultCheck1">
                                      {{ $data->name }}
                                    </label>
                                  </div>
                              @endforeach
                       
                    </div> 
                    <!-- /.qualification -->



                    </div>
                  </div>
              </div>



              <div class="card filter-card">
                  <div id="headingThree">
                    <h5 class="mb-0">
                      <a class=" collapsed"  data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                      <div class="down-arrow-icon my-2 mx-3 pl-1 d-flex flex-row justify-content-between"> 
                        <div>
                           <h6>Courses & Program</h6>
                        </div>
                        <div class="">
                        <i class="fas fa-sort-down"  id="down-arrow" id="down-arrow"></i>

                        </div>  
                       </div>
                       <!-- flex -->

                      </a>
                    </h5>
                  </div>

                  <div id="collapseThree" class="collapse " aria-labelledby="headingThree" data-parent="#accordion">
                    <div class="card-body">
                <!-- 3rd part -->
                <div class="courses">     
                      <!-- <h6 class="my-2">Courses & Program</h6> -->
                      <!-- 1st Check box -->
                      @php 
                            $CoursesPrograms = App\Http\Controllers\trastaven\FilterController::CoursesPrograms();
                          @endphp
                                  <!-- 1st Check box -->
                              @foreach ($CoursesPrograms as $data)
                                  <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="course_program[]" value="{{ $data->id }}" id="defaultCheck1" onchange="call()">
                                    <label class="form-check-label" for="defaultCheck1">
                                      {{ $data->name }}
                                    </label>
                                  </div>
                              @endforeach
                                               

                </div>
               <!-- /.courses -->

                    </div>
                  </div>
              </div>

              <div class="card filter-card">
                  <div id="headingFour">
                    <h5 class="mb-0">
                      <a class=" collapsed"  data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                      <div class="down-arrow-icon my-2 mx-3 pl-1 d-flex flex-row justify-content-between"> 
                        <div>
                           <h6> Department</h6>
                        </div>
                        <div class="">
                        <i class="fas fa-sort-down"  id="down-arrow" id="down-arrow"></i>

                        </div>  
                       </div>
                       <!-- flex -->


                      </h6>
                      </a>
                    </h5>
                  </div>

                <div id="collapseFour" class="collapse " aria-labelledby="headingFour"  data-parent="#accordion">
                    <div class="card-body">
                    <!--------- 4th part-------- -->
                     <div class="department">
              
                        <!-- <h6 class="my-2">Department</h6> -->
                        <!-- <h6 class="my-1">Medical :</h6> -->
                        <!-- 1st Check box -->
                       @php 
                            $Department = App\Http\Controllers\trastaven\FilterController::Department();
                          @endphp
                                  <!-- 1st Check box -->
                              @foreach ($Department as $data)
                                  <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="dept[]" value="{{ $data->id }}" id="defaultCheck1" onchange="call()">
                                    <label class="form-check-label" for="defaultCheck1">
                                      {{ $data->name }}
                                    </label>
                                  </div>
                              @endforeach

                       
                    </div>    
                   <!-- /.department -->


                    </div>
                </div>
              </div>


                
              <div class="card filter-card">
                  <div id="headingFive">
                    <h5 class="mb-0">
                      <a class=" collapsed"  data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                      <div class="down-arrow-icon my-2 mx-3 pl-1 d-flex flex-row justify-content-between"> 
                        <div>
                           <h6>Certificate & Degrees</h6>
                        </div>
                        <div class="">
                        <i class="fas fa-sort-down"  id="down-arrow" id="down-arrow"></i>

                        </div>  
                       </div>
                       <!-- flex -->

                      </a>
                    </h5>
                  </div>

                <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
                  <div class="card-body">
                          <!--------- 5th part-------- -->
                    <div class="certificate">
              
                      <!-- 1st Check box -->
                       @php 
                            $CertificatesDegrees = App\Http\Controllers\trastaven\FilterController::CertificatesDegrees();
                          @endphp
                                  <!-- 1st Check box -->
                              @foreach ($CertificatesDegrees as $data)
                                  <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="certificate_and_degrees[]" value="{{ $data->id }}" id="defaultCheck1" onchange="call()">
                                    <label class="form-check-label" for="defaultCheck1">
                                      {{ $data->name }}
                                    </label>
                                  </div>
                              @endforeach

                        <!-- 7th Check box -->

                        
                    </div>
                      <!-- /.certificate -->


                  </div>
                </div>
              </div>

              <div class="card filter-card">
                  <div id="headingSix">
                    <h5 class="mb-0">
                      <a class=" collapsed"  data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                      <div class="down-arrow-icon my-2 mx-3 pl-1 d-flex flex-row justify-content-between"> 
                        <div>
                           <h6>Location</h6>
                        </div>
                        <div class="">
                        <i class="fas fa-sort-down"  id="down-arrow" id="down-arrow"></i>

                        </div>  
                       </div>
                       <!-- flex -->

                      </a>
                    </h5>
                  </div>

                <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion">
                  <div class="card-body">
                        <!--------- 6th part-------- -->
                        <div class="location">
                        
                                <!-- <h6 class="my-2">Location</h6> -->
                                <!-- 1st Check box -->
                         @php 
                            $Location = App\Http\Controllers\trastaven\FilterController::Location();
                          @endphp
                                  <!-- 1st Check box -->
                              @foreach ($Location as $data)
                                  <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="location[]" value="{{ $data->id }}" id="defaultCheck1" onchange="call()">
                                    <label class="form-check-label" for="defaultCheck1">
                                      {{ $data->name }}
                                    </label>
                                  </div>
                              @endforeach
                                  


                        </div>
                        <!-- /.location -->


                  </div>
                </div>
              </div>


              <div class="card filter-card">
                  <div id="headingSeven">
                    <h5 class="mb-0">
                      <a class=" collapsed"  data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                      <div class="down-arrow-icon my-2 mx-3 pl-1 d-flex flex-row justify-content-between"> 
                        <div>
                           <h6> Affliated by</h6>
                        </div>
                        <div class="">
                        <i class="fas fa-sort-down"  id="down-arrow" id="down-arrow"></i>

                        </div>  
                       </div>
                       <!-- flex -->

                      </a>
                    </h5>
                  </div>

                 <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordion">
                    <div class="card-body">
                        <!-- -_-_-_- 8th part -->
                        <div class="affliate">
                              

                              <!-- <h6 class="my-2">Affliated by</h6> -->
                              <!-- 1st Check box -->
                          @php 
                            $AffiliatedBy = App\Http\Controllers\trastaven\FilterController::AffiliatedBy();
                          @endphp
                                  <!-- 1st Check box -->
                              @foreach ($AffiliatedBy as $data)
                                  <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="affiliated_by[]" value="{{ $data->id }}" id="defaultCheck1" onchange="call()">
                                    <label class="form-check-label" for="defaultCheck1">
                                      {{ $data->name }}
                                    </label>
                                  </div>
                              @endforeach



                        </div>
                      <!--/.affliated -->


                    </div>
                  </div>
              </div>


              <div class="card filter-card">
                  <div id="headingEight">
                    <h5 class="mb-0">
                      <a class=" collapsed"  data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                      <div class="down-arrow-icon my-2 mx-3 pl-1 d-flex flex-row justify-content-between"> 
                        <div>
                           <h6> Approved by</h6>
                        </div>
                        <div class="">
                        <i class="fas fa-sort-down"  id="down-arrow" id="down-arrow"></i>

                        </div>  
                       </div>
                       <!-- flex -->

                      </a>
                    </h5>
                  </div>

                <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordion">
                  <div class="card-body">
                        <!--------- 9th part-------- -->
                        <div class="approved">
                        
                                <!-- <h6 class="my-2">Approved by</h6> -->
                                <!-- 1st Check box -->
                                @php 
                            $ApprovedBy = App\Http\Controllers\trastaven\FilterController::ApprovedBy();
                          @endphp
                                  <!-- 1st Check box -->
                              @foreach ($ApprovedBy as $data)
                                  <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="approved_by[]" value="{{ $data->id }}" id="defaultCheck1" onchange="call()">
                                    <label class="form-check-label" for="defaultCheck1">
                                      {{ $data->name }}
                                    </label>
                                  </div>
                              @endforeach
                                


                        </div>
                        <!-- /.APPROVED -->


                  </div>
                </div>
              </div>


              <div class="card filter-card">
                  <div id="headingNine">
                    <h5 class="mb-0">
                      <a class=" collapsed"  data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                      <div class="down-arrow-icon my-2 mx-3 pl-1 d-flex flex-row justify-content-between"> 
                        <div>
                           <h6>Education loan </h6>
                        </div>
                        <div class="">
                        <i class="fas fa-sort-down"  id="down-arrow" id="down-arrow"></i>

                        </div>  
                       </div>
                       <!-- flex -->

                      </a>
                    </h5>
                  </div>

                <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordion">
                  <div class="card-body">
                      <!--------- 10th part-------- -->
                      <div class="loan">
                      
                              <h6 class="my-2"></h6>
                              <!-- 1st Check box -->
                               @php 
                            $EduLoan = App\Http\Controllers\trastaven\FilterController::EduLoan();
                          @endphp
                                  <!-- 1st Check box -->
                              @foreach ($EduLoan as $data)
                                  <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="edu_loan[]" value="{{ $data->id }}" id="defaultCheck1" onchange="call()">
                                    <label class="form-check-label" for="defaultCheck1">
                                      {{ $data->name }}
                                    </label>
                                  </div>
                              @endforeach

                              
                      </div>
                      <!-- /.loan -->


                  </div>
                </div>
              </div>




              <!-- ranking -->

                <div class="card filter-card">
                  <div id="headingTen">
                    <h5 class="mb-0">
                      <a class=" collapsed"  data-toggle="collapse" data-target="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                      <div class="down-arrow-icon my-2 mx-3 pl-1 d-flex flex-row justify-content-between"> 
                        <div>
                           <h6> Trastaven Ranking</h6>
                        </div>
                        <div class="">
                        <i class="fas fa-sort-down"  id="down-arrow" id="down-arrow"></i>

                        </div>  
                       </div>
                       <!-- flex -->

                      </a>
                    </h5>
                  </div>

                  <div id="collapseTen" class="collapse" aria-labelledby="headingTen" data-parent="#accordion">
                    <div class="card-body">
                         
                        <!--------- 11th part-------- -->
                        <div class="ranking">
                        
                                <!-- <h6 class="my-2">Trastaven Ranking</h6> -->
                                <!-- 1st Check box -->
                                <div class="form-check">
                                  <input class="form-check-input" type="checkbox" name="five" value="5" id="defaultCheck1" onchange="call()">
                                  <label class="form-check-label" for="defaultCheck1">
                                      <i class="fas fa-star"></i>
                                      <i class="fas fa-star"></i>
                                      <i class="fas fa-star"></i>
                                      <i class="fas fa-star"></i>
                                      <i class="fas fa-star"></i>
                                    </label>
                                </div>
                                  <!-- 2nd Check box -->
                                  <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="four" value="4" id="defaultCheck1" onchange="call()">
                                        <label class="form-check-label" for="defaultCheck1">
                                         <i class="fas fa-star"></i>
                                         <i class="fas fa-star"></i>
                                         <i class="fas fa-star"></i>
                                         <i class="fas fa-star"></i>

                                        </label>
                                  </div>

                                  <!-- 3rd Check box -->

                                  <div class="form-check">
                                  <input class="form-check-input" type="checkbox" name="three" value="3" id="defaultCheck1" onchange="call()">
                                  <label class="form-check-label" for="defaultCheck1">
                                      <i class="fas fa-star"></i>
                                      <i class="fas fa-star"></i>
                                      <i class="fas fa-star"></i>

                                  </label>
                                </div>
                                  <!-- 4th Check box -->
                                  <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="two" value="2" id="defaultCheck1" onchange="call()">
                                        <label class="form-check-label" for="defaultCheck1">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>

                                        </label>
                                  </div>

                                <!-- 5th Check box -->
                                <div class="form-check">
                                  <input class="form-check-input" type="checkbox" name="one" value="1" id="defaultCheck1"onchange="call()"> 
                                  <label class="form-check-label" for="defaultCheck1">
                                  <i class="fas fa-star"></i>
                                  </label>
                                </div>
                        </div>
                        <!-- /.ranking -->              


     </form> 
      <script type="text/javascript">
    function call(){
        document.getElementById("filterform").submit();
    }

      </script>

                    </div>
                  </div>
                
                </div>




             </div>

      <!-- /.accordion -->


       </div>  
            <!-- /.side-bar -->
  </div>
    <!-- /.col -->

 
