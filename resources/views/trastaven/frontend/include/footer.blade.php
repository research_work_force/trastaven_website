 <!--Footer Section -->
 <div class="container-fluid prefooter text-white">
           
             <div class="row"> 
                   <div class="col-sm-12 col-lg-2 text-left links mx-4">
                              <a href="/filter/courses"><h6>Prime cities</h6></a> 
                              <a href="/filter/courses">Kolkata</a>
                              <a href="/filter/courses">Bangalore</a>
                              <a href="/filter/courses">Hyderabad</a>
                              <a href="/filter/courses">Pune</a>
                              <a href="/filter/courses">Mumbai</a>
                              <a href="/filter/courses">Bhubeneshwar</a>
                              <a href="/filter/courses">Chandigarh</a>
                              <a href="/filter/courses">Delhi</a>
                              <a href="/filter/courses">Bhupal</a>
                              <a href="/filter/courses">Ranchi</a>
                              <a href="/filter/courses">Durgaur</a>
            
                   </div>
                   <div class="col-sm-12 col-lg-2 links text-left mx-4">
                        <a href="#"><h6>Courses</h6></a> 
                         <a href="/filter/courses">Btech/Mtech</a>
                         <a href="/filter/courses">BE</a>
                         <a href="/filter/courses">MBBS/MS/MD</a>
                         <a href="/filter/courses">BDS</a>
                         <a href="/filter/courses">BCA/MCA</a>
                         <a href="/filter/courses">BFA/MFA</a>
                   </div>
                   <div class="col-sm-12 col-lg-2 text-left links mx-4">
                              <a href="/filter/courses"><h6>Countries</h6></a> 
                              <a href="/filter/courses">USA</a>
                              <a href="/filter/courses">Germany</a>
                              <a href="/filter/courses">Russia</a>
                              <a href="/filter/courses">Ukraine</a>
                              <a href="/filter/courses">China</a>
                              <a href="/filter/courses">Nepal</a>
                              <a href="/filter/courses">Bangladesh</a>

                   </div>
                   <div class="col-sm-12 col-lg-2 text-left links trastaven-footer-col">
                              <a href="#"><h6>Partner Program</h6></a>
                              <a href="/contact">Become a Partner</a>

                              
                   </div>
                   <div class="col-sm-12 col-lg-2 text-left links trastaven-footer-col">
                              <a href="#"><h6>Other Links</h6></a>
                              <a href="/contact">Contact Us</a>
                              <a href="/aboutus">About Us</a>

                   </div>
                              
                  
              </div>
              <!-- /.row -->
</div>
<!-- /.prefooter -->
            <div class="container-fluid footer">
                  
                         <div class="text-center">
                                   <p class="mb-0 pt-1"> © 2019 <a style="color: #00b5ac;font-weight: bold !important;">Trastaven</a> Powered By <a> <a style="color: #00b5ac;font-weight: bold !important;">Think Again Lab.</a> </p>

                         </div>
            </div> 
       
                         <!-- side-toggle-bar -->
                         <!-- <div class="side-toggle-bar  p-0 ">
                                    <div class="toggle-portion p-0">
                                         <div class="side-toggle-banner">
                                                <a href="#"><i class="fas fa-user-tie"></i></a>
      
                                               <h6>Your Name</h6>
                                               <h6>Your@domain.name</h6>
                                         </div>
                                         <div class="side-toggle-menu">
                                               <div class="d-flex d-flex-row m-3">
                                                      <i class="fas fa-home mr-4 mt-1"></i>
                                                      <h6>Home</h6>
                                               </div> 
      
                                                <div class="d-flex d-flex-row m-3">
                                                       <i class="fas fa-home mr-4 mt-1"></i>
                                                       <h6>Home</h6>
                                                </div> 
      
                                                <div class="d-flex d-flex-row m-3">
                                                            <i class="fas fa-home mr-4 mt-1"></i>
                                                            <h6>Home</h6>
                                                </div> 
      
                                                <div class="d-flex d-flex-row m-3">
                                                            <i class="fas fa-home mr-4 mt-1"></i>
                                                            <h6>Home</h6>
                                                </div>          
      
                                                <div class="d-flex d-flex-row m-3">
                                                            <i class="fas fa-home mr-4 mt-1"></i>
                                                            <h6>Home</h6>
                                                </div> 
      
                                                <div class="d-flex d-flex-row m-3">
                                                            <i class="fas fa-home mr-4 mt-1"></i>
                                                            <h6>Home</h6>
                                                </div> 
      
                                                <div class="d-flex d-flex-row m-3">
                                                            <i class="fas fa-home mr-4 mt-1"></i>
                                                            <h6>Home</h6>
                                                </div>
                                                   <hr class="ml-3">                         
                                               <div class="d-flex d-flex-row m-3">
                                                            <i class="fas fa-home mr-4 mt-1"></i>
                                                            <h6>Home</h6>
                                                </div>   
      
                                                <div class="d-flex d-flex-row m-3">
                                                            <i class="fas fa-home mr-4 mt-1"></i>
                                                            <h6>Home</h6>
                                                </div>
                                          </div>
                              </div>
                     </div> -->
<!-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" crossorigin="anonymous"></script>
<script
    src="https://code.jquery.com/jquery-3.3.1.js"
    integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
    crossorigin="anonymous">
</script> -->