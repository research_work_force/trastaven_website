      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta http-equiv="X-UA-Compatible" content="ie=edge">
      
      <!-- google fonts -->
      <link href="https://fonts.googleapis.com/css?family=EB+Garamond:400,500,600,700,800&display=swap" rel="stylesheet">

     <!-- bootstrap 4 cdn -->
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

      <!-- bootstrap slider -->
     

       <!--fontawesome  -->
      <script src="https://kit.fontawesome.com/bd08ad0f6e.js"></script>
      <link href="https://fonts.googleapis.com/css?family=Satisfy&display=swap" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Lato:400,700,900&display=swap" rel="stylesheet">
      <link rel="stylesheet" href="{{ asset('trastaven/frontend/css/mainPage.css') }}">
      <link rel="stylesheet" href="{{ asset('trastaven/frontend/css/filter.css') }}">
      <link rel="stylesheet" href="{{ asset('trastaven/frontend/css/blog.css') }}">
      <link rel="stylesheet" href="{{ asset('trastaven/frontend/css/loan.css') }}">


