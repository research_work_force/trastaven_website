<!DOCTYPE html>
<html lang="en">
<head>
      @include('trastaven.frontend.include.headerlinks')
      <title>About Us</title>
</head>
<body>
      @include('trastaven.frontend.include.prenavbar')
      @include('trastaven.frontend.include.navbar')

      @yield('content')

      @include('trastaven.frontend.include.footer')
      @include('trastaven.frontend.include.footerlinks')
</body>

   <script>
$(document).ready(function() {
      $('.side-toggler-btn').on('click' , function(e){
            e.stopPropagation();
      $('.side-toggle-bar').addClass('open');
      $('.whole-div').addClass('darken-body');

      });
      $('.side-toggle-bar').on('click' , function(e){
      e.stopPropagation();
      });

      $('body,html').on('click' , function(e){
      $('.side-toggle-bar').removeClass('open');
      $('.whole-div').removeClass('darken-body');
      });
      
})

</script>
</html>