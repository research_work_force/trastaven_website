<!DOCTYPE html>
<html lang="en">
<head>
      @include('trastaven.frontend.include.headerlinks')
      <title>Trastaven</title>
</head>
<body>
      @include('trastaven.frontend.include.prenavbar')
      @include('trastaven.frontend.include.navbar')

      @yield('content')

      @include('trastaven.frontend.include.footer')
      
      @include('trastaven.frontend.include.footerlinks')
</body>

</html>