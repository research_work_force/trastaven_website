@extends('trastaven.layouts.add_subfilterslistlayout')

@section('content')
<div class="container-fluid mt--7">
      <!-- Table -->
      <div class="row">
        <div class="col">
        @if(Session::has('message'))
              <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
              @endif
          <div class="card shadow">
            <div class="card-header border-0">
           
              <h3 class="trastavenh3 mb-0">Sub-Filters List</h3>
             
            </div>
            <div class="table-responsive">
              <table class="table align-items-center table-flush">
                <thead class="thead-light">
                  <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Name</th>
                    <th scope="col"></th>
                  </tr>
                </thead>
                <tbody>
                @foreach($course_filter as $data)
                  <tr>
                    <th scope="row">
                      <!-- <div class="media align-items-center">
                        <a href="#" class="avatar rounded-circle mr-3">
                          <img alt="Image placeholder" src="../assets/img/theme/bootstrap.jpg">
                        </a> -->
                        <div class="media-body">
                         
                        <span class="mb-0 text-sm">{{ $data->id }}</span>
                         
                         
                         
                        </div>
                      <!-- </div> -->
                    </th>
                    <td>
                    
                        {{ $data->name }}
                    </td>
                    
                    <td class="text-right">
                      <div class="dropdown">
                        <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <i class="fas fa-ellipsis-v"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                          <!-- <a class="dropdown-item" href="/course/edit/{{ $data->id }}"><i class="fa fa-file-pdf-o" aria-hidden="true"></i>Edit</a> -->
                          <a class="dropdown-item" href="/filter/delete/{{ $data->id }}/{{$flag}}"><i class="fa fa-" aria-hidden="true"></i>Delete</a>
                        </div>
                      </div>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            {{ $course_filter->appends(['filter_types' => $flag])->render("pagination::bootstrap-4") }}
           
          </div>
        </div>
      </div>
     



@endsection