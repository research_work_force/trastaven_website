@extends('trastaven.layouts.courselistlayout')

@section('content')
<div class="container-fluid mt--7">
      <!-- Table -->
      <div class="row">
        <div class="col">
        @if(Session::has('message'))
              <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
              @endif
          <div class="card shadow">
            <div class="card-header border-0">
           
              <h3 class="trastavenh3 mb-0">Courses</h3>
             
            </div>
            <div class="table-responsive">
              <table class="table align-items-center table-flush">
                <thead class="thead-light">
                  <tr>
                    <th scope="col">Course Type</th>
                    <th scope="col">Course Fees &#x20B9;</th>
                    <th scope="col">Last Qualification</th>
                    <th scope="col">Course & Program</th>
                    <th scope="col">Dept</th>
                    <th scope="col">Certificate & Degrees</th>
                    <th scope="col">Place (Location)</th>
                    <th scope="col">Affiliated by</th>
                    <th scope="col">Approved_by </th>
                    <th scope="col">Edu Loan</th>
                    <th scope="col">College Name</th>
                    <th scope="col">Details</th>
                    <th scope="col">Rating</th>
                    <th scope="col"></th>
                  </tr>
                </thead>
                <tbody>
                @foreach($course_data as $data)
                  <tr>
                    <th scope="row">
                      <!-- <div class="media align-items-center">
                        <a href="#" class="avatar rounded-circle mr-3">
                          <img alt="Image placeholder" src="../assets/img/theme/bootstrap.jpg">
                        </a> -->
                        <div class="media-body">
                          @php 
                          $CourseType = App\Http\Controllers\trastaven\FilterController::CourseType();
                        @endphp
                                              
                        @foreach ($CourseType as $val)
                          @if($val->id == $data->course_type)
                        <span class="mb-0 text-sm">{{ $val->name }}</span>
                          @endif
                        @endforeach
                         
                         
                        </div>
                      <!-- </div> -->
                    </th>
                    <td>
                    @php 
                          $FeesRange = App\Http\Controllers\trastaven\FilterController::FeesRange();
                        @endphp
                                              
                        @foreach ($FeesRange as $val)
                          @if($val->id == $data->course_fees)
                        {{ $val->name }}
                          @endif
                        @endforeach
                    </td>
                    <td>
                      <span class="badge badge-dot mr-4">
                  
                         @php 
                          $LastQualification = App\Http\Controllers\trastaven\FilterController::LastQualification();
                        @endphp
                                              
                        @foreach ($LastQualification as $val)
                          @if($val->id == $data->last_qualification)
                        {{ $val->name }}
                          @endif
                        @endforeach
                     
                      </span>
                    </td>
                    <td>
                      <div class="avatar-group">

                      @php 
                          $CoursesPrograms = App\Http\Controllers\trastaven\FilterController::CoursesPrograms();
                        @endphp
                                              
                        @foreach ($CoursesPrograms as $val)
                          @if($val->id == $data->dept)
                        {{ $val->name }}
                          @endif
                        @endforeach
                        
                      </div>
                    </td>
                    <td>
                      <div class="d-flex align-items-center">
                        @php 
                          $Department = App\Http\Controllers\trastaven\FilterController::Department();
                        @endphp
                                              
                        @foreach ($Department as $val)
                          @if($val->id == $data->dept)
                        <span class="mr-2">{{ $val->name }}</span>
                          @endif
                        @endforeach
                        <div>
                          <!-- <div class="progress">
                            <div class="progress-bar bg-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"></div>
                          </div> -->
                        </div>
                      </div>
                    </td>
                    <td>
                      <div class="d-flex align-items-center">
                        @php 
                          $CertificatesDegrees = App\Http\Controllers\trastaven\FilterController::CertificatesDegrees();
                        @endphp
                                              
                        @foreach ($CertificatesDegrees as $val)
                          @if($val->id == $data->certificate_and_degrees)
                        <span class="mr-2">{{ $val->name }}</span>
                          @endif
                        @endforeach
                        <div>
                          <!-- <div class="progress">
                            <div class="progress-bar bg-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"></div>
                          </div> -->
                        </div>
                      </div>
                    </td>


                    <td>
                      <div class="d-flex align-items-center">
                        @php 
                          $Location = App\Http\Controllers\trastaven\FilterController::Location();
                        @endphp
                                              
                        @foreach ($Location as $val)
                          @if($val->id == $data->location)
                        <span class="mr-2">{{$data->place}} [{{ $val->name }}]</span>
                          @endif
                        @endforeach
                        <div>
                          <!-- <div class="progress">
                            <div class="progress-bar bg-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"></div>
                          </div> -->
                        </div>
                      </div>
                    </td>

                    <td>
                      <div class="d-flex align-items-center">
                        
                        @php 
                          $AffiliatedBy = App\Http\Controllers\trastaven\FilterController::AffiliatedBy();
                        @endphp
                                              
                        @foreach ($AffiliatedBy as $val)
                          @if($val->id == $data->affiliated_by)
                        <span class="mr-2">{{ $val->name }}</span>
                          @endif
                        @endforeach
                        <div>
                          <!-- <div class="progress">
                            <div class="progress-bar bg-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"></div>
                          </div> -->
                        </div>
                      </div>
                    </td>

                    <td>
                      <div class="d-flex align-items-center">
                        @php 
                          $ApprovedBy = App\Http\Controllers\trastaven\FilterController::ApprovedBy();
                        @endphp
                                              
                        @foreach ($ApprovedBy as $val)
                          @if($val->id == $data->approved_by)
                        <span class="mr-2">{{ $val->name }}</span>
                          @endif
                        @endforeach
                        <div>
                          <!-- <div class="progress">
                            <div class="progress-bar bg-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"></div>
                          </div> -->
                        </div>
                      </div>
                    </td>

                    <td>
                      <div class="d-flex align-items-center">
                         @php 
                          $EduLoan = App\Http\Controllers\trastaven\FilterController::EduLoan();
                        @endphp
                                              
                        @foreach ($EduLoan as $val)
                          @if($val->id == $data->eduloan)
                        <span class="mr-2">{{ $val->name }}</span>
                          @endif
                        @endforeach
                        <div>
                          <!-- <div class="progress">
                            <div class="progress-bar bg-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"></div>
                          </div> -->
                        </div>
                      </div>
                    </td>

                    <td>
                      <div class="d-flex align-items-center">
                        <span class="mr-2">{{ $data->college_name }}</span>
                        <div>
                          <!-- <div class="progress">
                            <div class="progress-bar bg-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"></div>
                          </div> -->
                        </div>
                      </div>
                    </td>

                    <td>
                      <div class="d-flex align-items-center">

                        <span class="mr-2">{{ $data->details }}</span>
                        <div>
                          <!-- <div class="progress">
                            <div class="progress-bar bg-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"></div>
                          </div> -->
                        </div>
                      </div>
                    </td>

                    <td>
                      <div class="d-flex align-items-center">
                       
                        <span class="mr-2">{{ $data->trating }}</span>
                        <div>
                          <!-- <div class="progress">
                            <div class="progress-bar bg-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"></div>
                          </div> -->
                        </div>
                      </div>
                    </td>

                    
                    <td class="text-right">
                      <div class="dropdown">
                        <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <i class="fas fa-ellipsis-v"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                          <!-- <a class="dropdown-item" href="/course/edit/{{ $data->id }}"><i class="fa fa-file-pdf-o" aria-hidden="true"></i>Edit</a> -->
                          <a class="dropdown-item" href="/course/delete/{{ $data->id }}"><i class="fa fa-" aria-hidden="true"></i>Delete</a>
                        </div>
                      </div>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            {{ $course_data->render("pagination::bootstrap-4") }}
           
          </div>
        </div>
      </div>
     



@endsection