@extends('trastaven.layouts.courselistlayout')

@section('content')

<!-- Page content -->
<form action="/course/save" method="post" enctype="multipart/form-data">
  <input type="hidden" value="{{csrf_token()}}" name="_token" id="token">
    <div class="container-fluid mt--7">
    @if(Session::has('message'))
              <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
              @endif
      <div class="row">
 
  <!--       <div class="col-xl-4 order-xl-2 mb-5 mb-xl-0">
          <div class="card card-profile shadow">
            <div class="row justify-content-center">
              <div class="col-lg-3 order-lg-2">
                <div class="card-profile-image">
                  <a href="#"> -->
                    <!-- <img src="{{ asset('trastaven/img/theme/team-4-800x800.jpg') }}" class="rounded-circle"> -->
                 <!--  </a>
                </div>
              </div>
            </div>
            <div class="card-header text-center border-0 pt-8 pt-md-4 pb-0 pb-md-4">
              <div class="d-flex justify-content-between">
               

              </div>
            </div>
            <div class="card-body pt-0 pt-md-4"> -->
              <!-- <input type="hidden" name="date" value="@php  $mytime = Carbon\Carbon::now(); 
                     echo $mytime; @endphp"> -->
              <!-- <div class="row">
                <div class="col">
                  <div class="card-profile-stats d-flex justify-content-center mt-md-5">
                    <div class="col-lg-12"> 
                     <div class="form-group">
                     <label class="form-control-label" for="input-address">Rate</label>
                        <input id="input-address" name="rate" class="form-control form-control-alternative" placeholder="Rate"  type="text" required>
                     </div>
                    </div>
                  </div>
                </div>
              </div> -->
              <!-- <hr class="my-4"> -->
             <!--  <div class="row">
                <div class="col">
                  <div class="card-profile-stats d-flex justify-content-center mt-md-5">
                    <div class="col-lg-12"> 
                     <div class="form-group">
                     <label class="form-control-label" for="input-address">Amount</label>
                        <input id="input-address" name="amt" class="form-control form-control-alternative" placeholder="Amount"  type="text" required>
                      </div>
                    </div>
                  </div>
                </div>
              </div> -->
              <!-- <hr class="my-4"> -->
              <!-- <div class="row">
                <div class="col">
                  <div class="card-profile-stats d-flex justify-content-center mt-md-5">
                    <div class="col-lg-12"> 
                     <div class="form-group">
                     <label class="form-control-label" for="input-address">Sub Total</label>
                        <input id="input-address" name="subtotal" class="form-control form-control-alternative" placeholder="Sub Total"  type="text" required>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col">
                  <div class="card-profile-stats d-flex justify-content-center mt-md-5">
                    <div class="col-lg-12"> 
                     <div class="form-group">
                     <label class="form-control-label" for="input-address">Due</label>
                        <input id="input-address" name="due" class="form-control form-control-alternative" placeholder="Due"  type="text" required>
                     </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col">
                  <div class="card-profile-stats d-flex justify-content-center mt-md-5">
                    <div class="col-lg-12"> 
                     <div class="form-group">
                     <label class="form-control-label" for="input-address">GST Type</label>
                        <select id="input-address" name="gst_type" class="form-control form-control-alternative" required>
                        <option value="0">Intra-State</option>
                        <option value="1">Inter-State</option>
                        </select>
                       </div>
                    </div>
                  </div>
                </div>
              </div> -->
              <!-- <hr class="my-4"> -->
              <!-- <div class="row">
                <div class="col">
                  <div class="card-profile-stats d-flex justify-content-center mt-md-5">
                    <div class="col-lg-12"> 
                     <div class="form-group">
                    

                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div> 
        </div> -->
        <div class="col-xl-12 order-xl-1" id='app'>
          <div class="card bg-secondary shadow">
            <div class="card-header bg-white border-0">
              <div class="row align-items-center">
                <div class="col-8">
                  <h3 class="trastavenh3 mb-0">Add Courses/Colleges</h3>
                </div>
                <div class="col-4 text-right">
                  <a href="/course/list" class="btn btn-sm btn-trastaven">See Lists</a>
                </div>
              </div>
            </div>
            <form action="/course/save" method="post">
            <div class="card-body">
                <!-- <h6 class="heading-small text-muted mb-4">User information</h6> -->
                <div class="pl-lg-4">
                  <div class="row">
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-address">Course Type</label>
                       <select class="form-control form-control-alternative" name="course_type" required>
                        @include('trastaven.pages.options.coursetype-opt')
                       </select>
                     
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-username">Course Fees</label>
                         <select class="form-control form-control-alternative" name="course_fees" required>
                        @include('trastaven.pages.options.feesrange')
                      </select>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-email">Last Qualification</label>
                        <select class="form-control form-control-alternative" name="last_qualification" required>
                        @include('trastaven.pages.options.last_qualification-opt')
                       </select>
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-address">Courses & Program</label>
                        <select class="form-control form-control-alternative" name="course_and_program" required>
                        @include('trastaven.pages.options.course_and_program-opt')
                       </select>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    
                    <div class="col-lg-6">
                      <div class="form-group">
                      <label class="form-control-label" for="input-address">Department</label>
                       <select class="form-control form-control-alternative" name="dept" required>
                        @include('trastaven.pages.options.dept-opt')
                       </select>
                     </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                      <label class="form-control-label" for="input-address">Certificate & Degrees</label>
                        <select class="form-control form-control-alternative" name="certificate_and_degrees" required>
                        @include('trastaven.pages.options.certificate_and_degrees-opt')
                       </select>
                     </div>
                    </div>
                  </div>
                </div>
                <!-- <hr class="my-4" /> -->
                <!-- Address -->
                <!-- <h6 class="heading-small text-muted mb-4">Contact information</h6> -->
                <div class="pl-lg-4">
                  <div class="row">
                    
                    <div class="col-md-6">
                      <div class="form-group">
                      <label class="form-control-label" for="input-address">Location</label>
                        <select class="form-control form-control-alternative" name="location" required>
                        @include('trastaven.pages.options.location-opt')
                       </select>
                     </div>
                    </div>


                    <div class="col-lg-6">
                      <div class="form-group">
                      <label class="form-control-label" for="input-address">Affliated by</label>
                      <select class="form-control form-control-alternative" name="affiliated_by" required>
                        @include('trastaven.pages.options.affiliated_by-opt')
                       </select>
                      </div>
                    </div>
                  </div>
                 
                  <div class="row">
                  <div class="col-lg-6">
                      <div class="form-group">
                      <label class="form-control-label" for="input-address">Approved by</label>
                      <select class="form-control form-control-alternative" name="approved_by" required>
                        @include('trastaven.pages.options.approved_by-opt')
                       </select>
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                      <label class="form-control-label" for="input-address">Education Load</label>
                      <select class="form-control form-control-alternative" name="eduloan" required>
                        @include('trastaven.pages.options.eduloan')
                       </select>
                      </div>
                    </div>
                    
                    </div>


                    <div class="row">
                  <div class="col-lg-6">
                      <div class="form-group">
                      <label class="form-control-label" for="input-address">College Name</label>
                      <input type="text" class="form-control form-control-alternative" placeholder="College Name" name="college_name" required>
                        
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                      <label class="form-control-label" for="input-address">Trastaven Rating</label>
                      <select class="form-control form-control-alternative" name="trating" required>
                        <option value="5">5</option>
                        <option value="4">4</option>
                        <option value="3">3</option>
                        <option value="2">2</option>
                        <option value="1">1</option>
                       </select>
                      </div>
                    </div>
                    
                    </div>

                    <div class="row">
                       <div class="col-md-6">
                      <div class="form-group">
                      <label class="form-control-label" for="input-address">PLace</label>
                         <input type="text" class="form-control form-control-alternative" placeholder="Place" name="place" required>
                     </div>
                    </div>
                    
                  <div class="col-lg-6">
                      <div class="form-group">
                      <label class="form-control-label" for="input-address">Details</label>
                      <textarea class="form-control form-control-alternative"type="text" placeholder="Write something here about this course" name="details" required></textarea>
                        
                      </div>
                    </div>
                    
                    </div>
                    
                  </div>
               

                <button type="submit" name="submit_type"  class="btn btn-sm btn-success float-right">Save</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    

@endsection