@extends('trastaven.layouts.courselistlayout')

@section('content')

<!-- Page content -->
<form action="/course/edit/update" method="post" enctype="multipart/form-data">
  <input type="hidden" value="{{csrf_token()}}" name="_token" id="token">
  <input type="hidden" value="{{ $course_data->id }}" name="id">
    <div class="container-fluid mt--7">
    @if(Session::has('message'))
              <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
              @endif
     <!--  <div class="row">
 
        <div class="col-xl-4 order-xl-2 mb-5 mb-xl-0">
          <div class="card card-profile shadow">
            <div class="row justify-content-center">
              <div class="col-lg-3 order-lg-2">
                <div class="card-profile-image">
                  <a href="#"> -->
                    <!-- <img src="{{ asset('trastaven/img/theme/team-4-800x800.jpg') }}" class="rounded-circle"> -->
                 <!--  </a>
                </div>
              </div>
            </div>
            <div class="card-header text-center border-0 pt-8 pt-md-4 pb-0 pb-md-4">
              <div class="d-flex justify-content-between">
               

              </div>
            </div>
            <div class="card-body pt-0 pt-md-4"> -->
              <!-- <input type="hidden" name="date" value="@php  $mytime = Carbon\Carbon::now(); 
                     echo $mytime; @endphp"> -->
              <!-- <div class="row">
                <div class="col">
                  <div class="card-profile-stats d-flex justify-content-center mt-md-5">
                    <div class="col-lg-12"> 
                     <div class="form-group">
                     <label class="form-control-label" for="input-address">Rate</label>
                        <input id="input-address" name="rate" class="form-control form-control-alternative" placeholder="Rate"  type="text" required>
                     </div>
                    </div>
                  </div>
                </div>
              </div> -->
              <!-- <hr class="my-4"> -->
             <!--  <div class="row">
                <div class="col">
                  <div class="card-profile-stats d-flex justify-content-center mt-md-5">
                    <div class="col-lg-12"> 
                     <div class="form-group">
                     <label class="form-control-label" for="input-address">Amount</label>
                        <input id="input-address" name="amt" class="form-control form-control-alternative" placeholder="Amount"  type="text" required>
                      </div>
                    </div>
                  </div>
                </div>
              </div> -->
              <!-- <hr class="my-4"> -->
              <!-- <div class="row">
                <div class="col">
                  <div class="card-profile-stats d-flex justify-content-center mt-md-5">
                    <div class="col-lg-12"> 
                     <div class="form-group">
                     <label class="form-control-label" for="input-address">Sub Total</label>
                        <input id="input-address" name="subtotal" class="form-control form-control-alternative" placeholder="Sub Total"  type="text" required>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col">
                  <div class="card-profile-stats d-flex justify-content-center mt-md-5">
                    <div class="col-lg-12"> 
                     <div class="form-group">
                     <label class="form-control-label" for="input-address">Due</label>
                        <input id="input-address" name="due" class="form-control form-control-alternative" placeholder="Due"  type="text" required>
                     </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col">
                  <div class="card-profile-stats d-flex justify-content-center mt-md-5">
                    <div class="col-lg-12"> 
                     <div class="form-group">
                     <label class="form-control-label" for="input-address">GST Type</label>
                        <select id="input-address" name="gst_type" class="form-control form-control-alternative" required>
                        <option value="0">Intra-State</option>
                        <option value="1">Inter-State</option>
                        </select>
                       </div>
                    </div>
                  </div>
                </div>
              </div> -->
              <!-- <hr class="my-4"> -->
              <!-- <div class="row">
                <div class="col">
                  <div class="card-profile-stats d-flex justify-content-center mt-md-5">
                    <div class="col-lg-12"> 
                     <div class="form-group">
                    

                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div> -->
        <div class="col-xl-12 order-xl-1" id='app'>
          <div class="card bg-secondary shadow">
            <div class="card-header bg-white border-0">
              <div class="row align-items-center">
                <div class="col-8">
                  <h3 class="trastavenh3 mb-0">Update Courses/Colleges</h3>
                </div>
                <div class="col-4 text-right">
                  <a href="/course/list" class="btn btn-sm btn-trastaven">See Lists</a>
                </div>
              </div>
            </div>
            <div class="card-body">
                <!-- <h6 class="heading-small text-muted mb-4">User information</h6> -->
                <div class="pl-lg-4">
                  <div class="row">
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-address">Course Name</label>
                        <input id="input-file" name="course_name" class="form-control form-control-alternative" placeholder="Course Name" value="{{ $course_data->course_name }}" type="text" required>
                     
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-username">Location</label>
                        <input id="input-file" name="location" class="form-control form-control-alternative" value="{{ $course_data->location }}" placeholder="Location"  type="text" required>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-email">College Name</label>
                        <input type="text" id="input-email" name="college_name" class="form-control form-control-alternative" value="{{ $course_data->college_name }}" placeholder="College Name">
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-address">Affiliated to</label>
                        <input id="input-address" name="affiliated_to" class="form-control form-control-alternative" value="{{ $course_data->affiliated_to }}" placeholder="Affiliated to"  type="text" required>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    
                    <div class="col-lg-6">
                      <div class="form-group">
                      <label class="form-control-label" for="input-address">Approved by</label>
                        <input id="input-qty" name="approved_by" class="form-control form-control-alternative" value="{{ $course_data->approved_by }}" placeholder="Approved by"  type="text" >
                     </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                      <label class="form-control-label" for="input-address">Under Which Group</label>
                        <input id="input-uwg"  name="under_which_group" class="form-control form-control-alternative" value="{{ $course_data->under_w_group }}" placeholder="Under Which Group"  type="text" required>
                     </div>
                    </div>
                  </div>
                </div>
                <!-- <hr class="my-4" /> -->
                <!-- Address -->
                <!-- <h6 class="heading-small text-muted mb-4">Contact information</h6> -->
                <div class="pl-lg-4">
                  <div class="row">
                    
                    <div class="col-md-6">
                      <div class="form-group">
                      <label class="form-control-label" for="input-address">Eligibility Criteria</label>
                        <input id="input-discount" name="eligibility_criteria" class="form-control form-control-alternative" value="{{ $course_data->eligibility_criteria }}" placeholder="Eligibility Criteria"  type="text" required >
                     </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                      <label class="form-control-label" for="input-address">Trastaven Gradetion</label>
                      <input id="input-tg" name="trastaven_gradetion" class="form-control form-control-alternative" value="{{ $course_data->trastaven_gradetion }}" placeholder="Trastaven Gradetion"  type="text" required onchange="caldisc()">
                      </div>
                    </div>
                  </div>
                  <h6 class="heading-small text-muted mb-4">Course Fees (Yearly)</h6>
                  <div class="row">
                  <div class="col-lg-3">
                      <div class="form-group">
                      <label class="form-control-label" for="input-address">1st Year</label>
                      <input id="input-yr" name="first_yr" class="form-control form-control-alternative" value="{{ $course_data->first_yr_fees }}" placeholder="1st Year"  type="text" required >
                     </div>
                    </div>
                    <div class="col-lg-3">
                      <div class="form-group">
                      <label class="form-control-label" for="input-address">2nd Year</label>
                      <input id="input-yr" name="second_yr" class="form-control form-control-alternative" value="{{ $course_data->second_yr_fees }}" placeholder="2nd Year"  type="text" required >
                    
                         </div>
                    </div>
                    <div class="col-lg-3">
                      <div class="form-group">
                      <label class="form-control-label" for="input-address">3rd Year</label>
                      <input id="input-yr" name="third_yr" class="form-control form-control-alternative" value="{{ $course_data->third_yr_fees }}" placeholder="3rd Year"  type="text" required >
                    
                         </div>
                    </div>
                    <div class="col-lg-3">
                      <div class="form-group">
                      <label class="form-control-label" for="input-address">4th Year</label>
                      <input id="input-yr" name="fourth_yr" class="form-control form-control-alternative" value="{{ $course_data->fourth_yr_fees }}" placeholder="4th Year"  type="text" required>
                    
                         </div>
                    </div>
                    
                  </div>
               

                <button type="submit" name="submit_type"  class="btn btn-sm btn-success float-right">Save</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    

@endsection