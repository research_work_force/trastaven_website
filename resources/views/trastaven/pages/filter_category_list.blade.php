@extends('trastaven.layouts.add_subfilterslistlayout')

@section('content')

<!-- Page content -->
<form action="/filters/list" method="get">
  <input type="hidden" value="{{csrf_token()}}" name="_token" id="token">
    <div class="container-fluid mt--7">
    @if(Session::has('message'))
              <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
              @endif
      <div class="row">
 
  <!--       <div class="col-xl-4 order-xl-2 mb-5 mb-xl-0">
          <div class="card card-profile shadow">
            <div class="row justify-content-center">
              <div class="col-lg-3 order-lg-2">
                <div class="card-profile-image">
                  <a href="#"> -->
                    <!-- <img src="{{ asset('trastaven/img/theme/team-4-800x800.jpg') }}" class="rounded-circle"> -->
                 <!--  </a>
                </div>
              </div>
            </div>
            <div class="card-header text-center border-0 pt-8 pt-md-4 pb-0 pb-md-4">
              <div class="d-flex justify-content-between">
               

              </div>
            </div>
            <div class="card-body pt-0 pt-md-4"> -->
              <!-- <input type="hidden" name="date" value="@php  $mytime = Carbon\Carbon::now(); 
                     echo $mytime; @endphp"> -->
              <!-- <div class="row">
                <div class="col">
                  <div class="card-profile-stats d-flex justify-content-center mt-md-5">
                    <div class="col-lg-12"> 
                     <div class="form-group">
                     <label class="form-control-label" for="input-address">Rate</label>
                        <input id="input-address" name="rate" class="form-control form-control-alternative" placeholder="Rate"  type="text" required>
                     </div>
                    </div>
                  </div>
                </div>
              </div> -->
              <!-- <hr class="my-4"> -->
             <!--  <div class="row">
                <div class="col">
                  <div class="card-profile-stats d-flex justify-content-center mt-md-5">
                    <div class="col-lg-12"> 
                     <div class="form-group">
                     <label class="form-control-label" for="input-address">Amount</label>
                        <input id="input-address" name="amt" class="form-control form-control-alternative" placeholder="Amount"  type="text" required>
                      </div>
                    </div>
                  </div>
                </div>
              </div> -->
              <!-- <hr class="my-4"> -->
              <!-- <div class="row">
                <div class="col">
                  <div class="card-profile-stats d-flex justify-content-center mt-md-5">
                    <div class="col-lg-12"> 
                     <div class="form-group">
                     <label class="form-control-label" for="input-address">Sub Total</label>
                        <input id="input-address" name="subtotal" class="form-control form-control-alternative" placeholder="Sub Total"  type="text" required>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col">
                  <div class="card-profile-stats d-flex justify-content-center mt-md-5">
                    <div class="col-lg-12"> 
                     <div class="form-group">
                     <label class="form-control-label" for="input-address">Due</label>
                        <input id="input-address" name="due" class="form-control form-control-alternative" placeholder="Due"  type="text" required>
                     </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col">
                  <div class="card-profile-stats d-flex justify-content-center mt-md-5">
                    <div class="col-lg-12"> 
                     <div class="form-group">
                     <label class="form-control-label" for="input-address">GST Type</label>
                        <select id="input-address" name="gst_type" class="form-control form-control-alternative" required>
                        <option value="0">Intra-State</option>
                        <option value="1">Inter-State</option>
                        </select>
                       </div>
                    </div>
                  </div>
                </div>
              </div> -->
              <!-- <hr class="my-4"> -->
              <!-- <div class="row">
                <div class="col">
                  <div class="card-profile-stats d-flex justify-content-center mt-md-5">
                    <div class="col-lg-12"> 
                     <div class="form-group">
                    

                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div> 
        </div> -->
        <div class="col-xl-12 order-xl-1" id='app'>
          <div class="card bg-secondary shadow">
            <div class="card-header bg-white border-0">
              <div class="row align-items-center">
                <div class="col-8">
                  <h3 class="trastavenh3 mb-0">Add Sub-filters</h3>
                </div>
                <div class="col-4 text-right">
                 
                </div>
              </div>
            </div>
            <div class="card-body">
                <!-- <h6 class="heading-small text-muted mb-4">User information</h6> -->
                <div class="pl-lg-4">
                  <div class="row">
                    <div class="col-lg-12">
                      <div class="form-group">
                        <label class="form-control-label" for="input-address">Filter Type</label>
                       <select class="form-control form-control-alternative" name="filter_types" required>
                       <option value="1">Course Type</option>
                       <option value="2">Course Fees</option>
                       <option value="3">Last Qualification</option>
                       <option value="4">Courses & Program</option>
                       <option value="5">Department</option>
                       <option value="6">Certificate & Degrees</option>
                       <option value="7">Location</option>
                       <option value="8">Affliated by</option>
                       <option value="9">Approved by</option>
                       <option value="10">Education Loan</option>                       
                       </select>
                      </div>
                    </div>
                  </div>
                   
                    
                  </div>
               

                <button type="submit" name="submit_type"  class="btn btn-sm btn-success float-right">Next</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    

@endsection