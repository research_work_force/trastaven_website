@extends('trastaven.layouts.addbloglayout')

@section('content')

<!-- Page content -->
<form action="/blog/save" method="post" enctype="multipart/form-data">
  <input type="hidden" value="{{csrf_token()}}" name="_token" id="token">
    <div class="container-fluid mt--7">
    @if(Session::has('message'))
              <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
              @endif
      <div class="row">

        <div class="col-xl-4 order-xl-2 mb-5 mb-xl-0">
          <div class="card card-profile shadow">
            <div class="row justify-content-center">
              <div class="col-lg-3 order-lg-2">
                <div class="card-profile-image">
                  <a href="#">
                    <!-- <img src="{{ asset('trastaven/img/theme/team-4-800x800.png') }}" class="rounded-circle"> -->
                  </a>
                </div>
              </div>
            </div>
            <div class="card-header text-center border-0 pt-8 pt-md-4 pb-0 pb-md-4">
              <div class="d-flex justify-content-between">
                <button type="submit" name="submit_type" value="draft" class="btn btn-sm btn-warning mr-4">Draft</button>
                <button type="submit" name="submit_type" value="published" class="btn btn-sm btn-success float-right">Publish</button>
              </div>
            </div>
            <div class="card-body pt-0 pt-md-4">
              <!-- <input type="hidden" name="date" value="@php  $mytime = Carbon\Carbon::now(); 
                     echo $mytime; @endphp"> -->
              <div class="row">
                <div class="col">
                  <div class="card-profile-stats d-flex justify-content-center mt-md-5">
                    <div class="col-lg-12"> 
                     <div class="form-group">
                        <label class="form-control-label" for="input-address">Categories</label>
                        <input id="input-address" name="post_categories" class="form-control form-control-alternative" placeholder="Categories(e.g. computer,hardware)"  type="text" required>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- <hr class="my-4"> -->
              <div class="row">
                <div class="col">
                  <div class="card-profile-stats d-flex justify-content-center mt-md-5">
                    <div class="col-lg-12"> 
                     <div class="form-group">
                        <label class="form-control-label" for="input-address">Tags</label>
                        <input id="input-address" name="post_tags" class="form-control form-control-alternative" placeholder="Tags (e.g. education,thoughts)"  type="text" required>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- <hr class="my-4"> -->
              <div class="row">
                <div class="col">
                  <div class="card-profile-stats d-flex justify-content-center mt-md-5">
                    <div class="col-lg-12"> 
                     <div class="form-group">
                        <label class="form-control-label" for="input-address">Excerpt</label>
                        <textarea rows="5" name="post_excerpt" class="form-control form-control-alternative" placeholder="Write your Excerpt ...." maxlength="150" ></textarea>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- <hr class="my-4"> -->
              <div class="row">
                <div class="col">
                  <div class="card-profile-stats d-flex justify-content-center mt-md-5">
                    <div class="col-lg-12"> 
                     <div class="form-group">
                        <label class="form-control-label" for="input-address">Featured Image</label>
                        <input id="input-file" name="featured_img" class="form-control form-control-alternative" placeholder="Tags (e.g. education,thoughts)"  type="file" required>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xl-8 order-xl-1">
          <div class="card bg-secondary shadow">
            <div class="card-header bg-white border-0">
              <div class="row align-items-center">
                <div class="col-8">
                  <h3 class="trastavenh3 mb-0">Create Your Blog</h3>
                  
                </div>
                <div class="col-4 text-right">
                  <a href="/blog/list" class="btn btn-sm btn-trastaven">See Lists</a>
                </div>
              </div>
            </div>
            <div class="card-body">
                <!-- <h6 class="heading-small text-muted mb-4">User information</h6> -->
                <div class="pl-lg-4">
                  <div class="row">
                    <div class="col-lg-12">
                      <div class="form-group">
                        <label class="form-control-label" for="input-username"></label>
                        <input type="text" id="input-slug" name="post_title" class="form-control form-control-alternative post" placeholder="Post Title" required>
                      </div>
                    </div>
                    <div class="col-lg-12">
               <!--  <p class="form-control-label"  for="input-text">Permalink: <a id="input-text" href="">http://localhost:8000/blog/</a></p> -->
                    </div>
                   <!--  <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-email">Email address</label>
                        <input type="email" id="input-email" class="form-control form-control-alternative" placeholder="jesse@example.com">
                      </div>
                    </div> -->
                  </div>
                  <div class="row">
                    <!-- <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-first-name">First name</label>
                        <input type="text" id="input-first-name" class="form-control form-control-alternative" placeholder="First name" value="Lucky">
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label" for="input-last-name">Last name</label>
                        <input type="text" id="input-last-name" class="form-control form-control-alternative" placeholder="Last name" value="Jesse">
                      </div>
                    </div> -->
                  </div>
                </div>
                <!-- <hr class="my-4" /> -->
                <!-- Address -->
                <!-- <h6 class="heading-small text-muted mb-4">Contact information</h6> -->
                <div class="pl-lg-4">
                  <div class="row">
                    <!-- <div class="col-md-12">
                      <div class="form-group">
                        <label class="form-control-label" for="input-address">Address</label>
                        <input id="input-address" class="form-control form-control-alternative" placeholder="Home Address" value="Bld Mihail Kogalniceanu, nr. 8 Bl 1, Sc 1, Ap 09" type="text">
                      </div>
                    </div> -->
                  </div>
                  <div class="row">
                    <!-- <div class="col-lg-4">
                      <div class="form-group">
                        <label class="form-control-label" for="input-city">City</label>
                        <input type="text" id="input-city" class="form-control form-control-alternative" placeholder="City" value="New York">
                      </div>
                    </div> -->
                    <div class="col-lg-4">
                      <!-- <div class="form-group">
                        <label class="form-control-label" for="input-country">Country</label>
                        <input type="text" id="input-country" class="form-control form-control-alternative" placeholder="Country" value="United States">
                      </div> -->
                    </div>
                    <div class="col-lg-4">
                      <!-- <div class="form-group">
                        <label class="form-control-label" for="input-country">Postal code</label>
                        <input type="number" id="input-postal-code" class="form-control form-control-alternative" placeholder="Postal code">
                      </div> -->
                    </div>
                  </div>
                </div>
                <hr class="my-4" />
                <!-- Description -->
                <!-- <h6 class="heading-small text-muted mb-4">About me</h6> -->
                <div class="pl-lg-4">
                  <div class="form-group">
                    <!-- <label>About Me</label> -->
                    <textarea rows="30" name="post_content" id="editor" class="form-control form-control-alternative"  placeholder="Write your thoughts ...." ></textarea>
                      </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>

@endsection