 <!-- Core -->
  <script src="{{ asset('trastaven/vendor/jquery/dist/jquery.min.js') }}"></script>
  <script src="{{ asset('trastaven/vendor/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script>
  <!-- Optional JS -->
  <script src="{{ asset('trastaven/vendor/chart.js/dist/Chart.min.js') }}"></script>
  <script src="{{ asset('trastaven/vendor/chart.js/dist/Chart.extension.js') }}"></script>
  <!-- Argon JS -->
  <script src="{{ asset('trastaven/js/argon.js?v=1.0.0') }}"></script>