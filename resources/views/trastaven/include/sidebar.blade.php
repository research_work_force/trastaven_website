 <!-- Sidenav -->
  <nav class="navbar navbar-vertical fixed-left navbar-expand-md navbar-light bg-white" id="sidenav-main">
    <div class="container-fluid">
      <!-- Toggler -->
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <!-- Brand -->
      <a class="navbar-brand pt-0" href="#">
        <img src="{{ asset('trastaven/img/brand/logo.png') }}" class="navbar-brand-img" alt="...">
      </a>
      <!-- User -->
      <ul class="nav align-items-center d-md-none">
        <li class="nav-item dropdown">
          <a class="nav-link nav-link-icon" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="ni ni-bell-55"></i>
          </a>
          <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right" aria-labelledby="navbar-default_dropdown_1">
            <a class="dropdown-item" href="#">Action</a>
            <a class="dropdown-item" href="#">Another action</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="#">Something else here</a>
          </div>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <div class="media align-items-center">
              <span class="avatar avatar-sm rounded-circle">
                <img alt="Image placeholder" src="./assets/img/theme/team-1-800x800.jpg">
              </span>
            </div>
          </a>
          <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
            <div class=" dropdown-header noti-title">
              <h6 class="text-overflow m-0">Welcome!</h6>
            </div>
            <a href="./examples/profile.html" class="dropdown-item">
              <i class="ni ni-single-02"></i>
              <span>My profile</span>
            </a>
            <a href="./examples/profile.html" class="dropdown-item">
              <i class="ni ni-settings-gear-65"></i>
              <span>Settings</span>
            </a>
            <a href="./examples/profile.html" class="dropdown-item">
              <i class="ni ni-calendar-grid-58"></i>
              <span>Activity</span>
            </a>
            <a href="./examples/profile.html" class="dropdown-item">
              <i class="ni ni-support-16"></i>
              <span>Support</span>
            </a>
            <div class="dropdown-divider"></div>
            <a href="#!" class="dropdown-item">
              <i class="ni ni-user-run"></i>
              <span>Logout</span>
            </a>
          </div>
        </li>
      </ul>
      <!-- Collapse -->
      <div class="collapse navbar-collapse" id="sidenav-collapse-main">
        <!-- Collapse header -->
        <div class="navbar-collapse-header d-md-none">
          <div class="row">
            <div class="col-6 collapse-brand">
              <a href="./index.html">
                <img src="./assets/img/brand/blue.png">
              </a>
            </div>
            <div class="col-6 collapse-close">
              <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle sidenav">
                <span></span>
                <span></span>
              </button>
            </div>
          </div>
        </div>
        <!-- Form -->
        <form class="mt-4 mb-3 d-md-none">
          <div class="input-group input-group-rounded input-group-merge">
            <input type="search" class="form-control form-control-rounded form-control-prepended" placeholder="Search" aria-label="Search">
            <div class="input-group-prepend">
              <div class="input-group-text">
                <span class="fa fa-search"></span>
              </div>
            </div>
          </div>
        </form> 
        <!-- Navigation -->
        <ul class="navbar-nav">
          <li class="nav-item {{ Request::is('blog') || Request::is('blog/list')  ? 'active' : '' }}">
            <a class="nav-link text-trastaven{{ Request::is('blog') || Request::is('blog/list')  ? 'active' : '' }}" href="/blog">
              <i class="ni ni-ruler-pencil text-trastaven{{ Request::is('blog') || Request::is('blog/list')  ? 'active' : 'notactive' }}"></i>Blog
            </a>
          </li>
          <li class="nav-item {{ Request::is('admission/list') ? 'active' : '' }}">
            <a class="nav-link text-trastaven{{ Request::is('admission/list') ? 'active' : '' }}" href="/admission/list">
              <i class="fa fa-envelope-open-text text-trastaven{{ Request::is('admission/list')  ? 'active' : 'notactive' }}"></i>Admission Request List
            </a>
          </li>
          <li class="nav-item {{ Request::is('add/course') || Request::is('course/list') ? 'active' : '' }}">
            <a class="nav-link text-trastaven{{ Request::is('add/course') || Request::is('course/list') ? 'active' : '' }}" href="/add/course">
              <i class="fa fa-plus-square text-trastaven{{ Request::is('add/course') || Request::is('course/list')  ? 'active' : 'notactive' }}"></i>Add Courses/Colleges
            </a>
          </li>
          <li class="nav-item {{ Request::is('filters') ? 'active' : '' }}">
            <a class="nav-link text-trastaven{{ Request::is('filters') ? 'active' : '' }}" href="/filters">
              <i class="fa fa-comments text-trastaven{{ Request::is('filters')  ? 'active' : 'notactive' }}"></i>Add Filters
            </a>
          </li>
          <li class="nav-item {{ Request::is('contact/messages/list') ? 'active' : '' }}">
            <a class="nav-link text-trastaven{{ Request::is('contact/messages/list') ? 'active' : '' }}" href="/contact/messages/list">
              <i class="fa fa-comments text-trastaven{{ Request::is('contact/messages/list')  ? 'active' : 'notactive' }}"></i> Contact Message List
            </a>
          </li>
         <!--  <li class="nav-item">
            <a class="nav-link" href="./examples/tables.html">
              <i class="ni ni-bullet-list-67 text-red"></i> Tables
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="./examples/login.html">
              <i class="ni ni-key-25 text-info"></i> Login
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="./examples/register.html">
              <i class="ni ni-circle-08 text-pink"></i> Register
            </a>
          </li> -->
        </ul>
        <!-- Divider -->
        <hr class="my-3">
        <!-- Heading -->
        <!-- <h6 class="navbar-heading text-muted">Documentation</h6> -->
        <!-- Navigation -->
        <ul class="navbar-nav mb-md-3">
         <!--  <li class="nav-item">
            <a class="nav-link" href="https://demos.creative-tim.com/argon-dashboard/docs/getting-started/overview.html">
              <i class="ni ni-spaceship"></i> Getting started
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="https://demos.creative-tim.com/argon-dashboard/docs/foundation/colors.html">
              <i class="ni ni-palette"></i> Foundation
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="https://demos.creative-tim.com/argon-dashboard/docs/components/alerts.html">
              <i class="ni ni-ui-04"></i> Components
            </a>
          </li> -->
        </ul>
      </div>
    </div>
  </nav>