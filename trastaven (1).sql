-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 26, 2019 at 01:14 PM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.0.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `trastaven`
--

-- --------------------------------------------------------

--
-- Table structure for table `admission_request`
--

CREATE TABLE `admission_request` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `msg` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `institute_code` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `course_code` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `admission_request`
--

INSERT INTO `admission_request` (`id`, `name`, `email`, `phone`, `address`, `msg`, `institute_code`, `course_code`, `date`) VALUES
(1, 'Jack Ma', 'jack@gmail.com', '796541230', 'denmark', 'i want to know more', '002', '001', '2019-06-17 13:07:17');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `comment_post_id` bigint(20) NOT NULL,
  `comment_author` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_author_email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_date` datetime NOT NULL,
  `comment_content` varchar(999) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE `courses` (
  `id` int(11) NOT NULL,
  `course_name` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `location` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `college_name` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `affiliated_to` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `approved_by` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `under_w_group` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `eligibility_criteria` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `trastaven_gradetion` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `first_yr_fees` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `second_yr_fees` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `third_yr_fees` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `fourth_yr_fees` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`id`, `course_name`, `location`, `college_name`, `affiliated_to`, `approved_by`, `under_w_group`, `eligibility_criteria`, `trastaven_gradetion`, `first_yr_fees`, `second_yr_fees`, `third_yr_fees`, `fourth_yr_fees`, `created_at`, `updated_at`) VALUES
(2, 'VBD', 'Jadavpur', 'HLPO', 'SDTRE', 'Sayandeep Majumdar', 'skjs', 'fol', 's', '1000', '2000', '3000', '4000', '2019-06-24 00:51:11', '2019-06-24 00:51:11');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2019_05_20_100836_create_posts_table', 1),
(3, '2019_05_20_102034_create_comments_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `post_author` bigint(20) DEFAULT NULL,
  `post_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `post_featured_img` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `post_excerpt` varchar(155) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `post_status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `post_content` varchar(999) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `post_date` datetime DEFAULT NULL,
  `comment_status` int(11) DEFAULT NULL,
  `comment_count` int(11) DEFAULT NULL,
  `post_slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `categories` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tags` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `post_author`, `post_title`, `post_featured_img`, `post_excerpt`, `post_status`, `post_content`, `post_date`, `comment_status`, `comment_count`, `post_slug`, `categories`, `tags`, `created_at`, `updated_at`) VALUES
(1, 1, 'h m', 'h m.jpg', '<p>asasas</p>', 'draft', '<h2 style=\"text-align: center;\">Superman is Hope for World.</h2>\r\n<p>I am a big fan of Superman. It\'s a Dark Comic Universe Character. It has a tremendous strong ability to defeat any other villain or any characters.&nbsp;</p>\r\n<p>&nbsp;</p>', '2019-05-24 16:39:15', 0, 0, 'h-m', 'jksdkjsd', 'ddd', '2019-05-24 16:39:15', '2019-05-24 16:39:15'),
(2, 1, 'i am', 'i am.jpg', '<p>asas</p>', 'published', '<figure class=\"image align-left\"><img src=\"http://www.thinkagainlab.com/bower_components/AdminLTE/dist/img/logotal.png\" alt=\"hi\" width=\"100\" height=\"41\" />\r\n<figcaption>Caption</figcaption>\r\n</figure>\r\n<p>&nbsp; nsaajknjksdn</p>\r\n<p>&nbsp;</p>', '2019-05-25 13:34:57', 0, 0, 'i-am', 'a', 's', '2019-05-25 13:34:57', '2019-05-25 13:34:57'),
(3, 1, 'sbjshdjkhsd df', 'sbjshdjkhsd df.jpg', 'snd,mns,dmsd', 'published', '<p>msndnsmdnmsnd&nbsp;<strong>skdlskdlksdlksd</strong></p>', '2019-05-29 15:34:58', 0, 0, 'sbjshdjkhsd-df', 'jksdkjsd', 'sd', '2019-05-29 15:34:58', '2019-05-29 15:34:58'),
(4, 1, 'jkdjjdd djkdk', 'jkdjjdd djkdk.jpg', 'nmnd', 'published', '<p>jdnkjdnkjdn&nbsp;<strong>kslms</strong></p>', '2019-05-29 15:37:25', 0, 0, 'jkdjjdd-djkdk', 's,s', 's', '2019-05-29 15:37:25', '2019-05-29 15:37:25'),
(5, 1, 'sbahjbasjkbkjsadnk', 'sbahjbasjkbkjsadnk.jpg', 'kabsdk', 'published', '<p>sjdknkjdsnkjdns&nbsp;<strong>sjhdkshdkj</strong></p>', '2019-06-11 23:55:54', 0, 0, 'sbahjbasjkbkjsadnk', 'sd', 'sd', '2019-06-11 23:55:54', '2019-06-11 23:55:54'),
(6, 1, 'hey dude', 'hey dude.png', 'jnnsjnnsd jdkhksd', 'published', '<p><strong>sjdjlsdljsldjskld dgdgd</strong> jsnkjlsdjknsd</p>', '2019-06-19 17:49:24', 0, 0, 'hey-dude', 'shdjksdsjdsd,', 'god,lop', '2019-06-19 17:49:24', '2019-06-19 17:49:24'),
(9, 1, 'sndsndm dos', 'sndsndm-dos.png', 'sdsdsds sd', 'published', '<p>kjsand&nbsp;<strong>aljjsa&nbsp;</strong>kssd</p>', '2019-06-19 18:00:54', 0, 0, 'sndsndm-dos', 'sd', 'sd', '2019-06-19 18:00:54', '2019-06-19 18:00:54');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_status` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `user_name`, `user_password`, `user_email`, `user_status`, `created_at`, `updated_at`, `remember_token`) VALUES
(1, 'Sayandeep Majumdar', 'sayan@gmail.com', '$2y$10$V1lz0HUQvoFWNa0x4U3Jv.vyWe.Vxb3zTB7maHly5YOFyW/Onwc3q', 'sayan@gmail.com', 1, '2019-05-24 15:15:21', '2019-05-24 15:15:21', NULL),
(3, 'Sayandeep Majumdar', 'dd@gmail.com', '$2y$10$TIGTsUnYIg.Pg5tbV/nopORTNlcqjVafUqF57ImueUssCSeV8RTu6', 'dd@gmail.com', 1, '2019-05-24 15:45:14', '2019-05-24 15:45:14', NULL),
(4, 'Sayandeep Majumdar', 'cc@gmail.com', '$2y$10$Q3et3WfWyXM9p6kFp5sBi.Nnw7wQWU38pXVHM4a6xLLqzmVRM5r6q', 'cc@gmail.com', 1, '2019-05-24 15:45:53', '2019-05-24 15:45:53', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admission_request`
--
ALTER TABLE `admission_request`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admission_request`
--
ALTER TABLE `admission_request`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
